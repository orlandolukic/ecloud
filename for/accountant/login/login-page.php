<?php

// Silence is golden!
if ( !defined( 'APP_REQ' ) )
    exit();

use Core\Database\GeneralTableNames;
use Core\Database\MaxAggregateFunction;
use Core\Database\OrderByClause;
use Core\Database\SelectQuery;
use Core\Database\WhereClause;
use Core\Database\WherePart;

class LoginPage extends \Core\Page {

    public int $a;

    public function __construct() {
        parent::__construct( "login" );
        $this->a = 199;
        add_action( 'core_database_ecloud_connected', 10, array( $this, 'connection' ) );
        $this->addStyleResource("main", "/style.css");
    }

    public function connection( PDO $connection ): void {

        try {
            $q = new SelectQuery(array(
                "fields" => array( 'name', 'surname', 'email', array(
                    "name" => 'username',
                    "alias" => "username123"
                ) ),
                "table" => GeneralTableNames::USERS
            ));
            $s = &$this;
//            if ( !IS_AJAX ) {
//                //echo $q->getQueryString();
//                $q->forEach(function($el) use(&$s) {
//                    //$this->a++;
//                    echo json_encode($el);
//                });
//            }
            $s = array();
            $q->forEach(function($el) use(&$s) {
                $s[] = $el;
            });
        } catch (Exception $e) {
            throw $e;
        }

        /*
        $statement = $connection->prepare(
            "SELECT * FROM users WHERE username = ?" );
        $statement->execute( array(
            'orlandolukic'
        ) );
        $arr = $statement->fetchAll();
        echo json_encode($arr);
        */
        /*for ($i = 0; $i< count($arr); $i++)
            echo json_encode($arr[$i]);
        */
    }

    public function getControllerName(): ?string
    {
        return "LoginController";
    }

    public function getTemplateUrl(): string
    {
        return __DIR__ . "/template.php";
    }

    public function init(): void
    {
        
    }

    public function canActivate(): bool
    {
        return true;
    }

    public function getTitleTranslationKey(): string
    {
        return 'accountant.login.title';
    }
}

// Create login page
\Core\PageManager::assignCurrentPage(new LoginPage());