/**
 * Login controller, login request handling
 *
 * @param $
 * @param core
 * @returns {*}
 * @constructor
 */
core.controllers.LoginController = function ($, core) {

    let c = core.createController();
    let buttonLogin;
    let submitForm;
    let beforeSubmitForm;

    /**
     * Handles fields check.
     *
     * @param form
     */
    beforeSubmitForm = function (form) {

    }

    /**
     * Handles form submission.
     * @param form
     */
    submitForm = function (form) {
        form.getButtonGroup("primary")
            .setEnabled(false)
            .setLoading(true);

        let a = ['top-left', 'top-center', 'top-right', 'bottom-right', 'bottom-center', 'bottom-left'];
        let b = ['warning', 'success', 'info', 'danger'];
        for (let i = 0; i<30; i++) {
            setTimeout(() => {
                let mssg = core.managers.MessageCenterManager.pushMessage({
                    position: a[i % 6],
                    type: b[i % 4],
                    onCreate: function( el ) {

                        el.getTitleArea().write("Ups dogodila se greska!");
                        el.getTitleArea().addMargin("bottom", 20);
                        el.getTextArea().write(
                            "Proin eu sodales diam. Curabitur laoreet tincidunt risus, sit amet fringilla risus convallis nec. "
                        );
                    },
                    title: "Ups... Dogodila se greska!",
                    text: "Ovo je tekst poruke",
                    timeout: {
                        duration: 5000
                    }
                });
                setTimeout( () => {
                    mssg.dismissMessage();
                }, 2000 );
            }, 500 + 500*i);
        }
    };

    c.onPageOpen = function (payload) {

        let form = new core.modules.FormModule($, core);
        form.addField("username", "f-username");
        form.addField("password", "f-password");

        form.createButtonGroup("primary")
            .addButton(".login-button")
            .setAction("click", function(e) {
                form.submit();
            });
        form.init({
            enableEnter: true,
            onLastSubmit: true,
            onSubmit: submitForm,
            onBeforeSubmit: beforeSubmitForm
        });

        // setInterval(() => {
        //     core.managers.MessageCenterManager.pushMessage("top-center", function(el) {
        //         el.addClass("ordinary-message").addClass('background-color-pure-white');
        //         el.html("CAOCAO");
        //     });
        // }, 1000);
        let mssgId;


    }
    return c;
}