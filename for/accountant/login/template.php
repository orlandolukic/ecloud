<div class="login-container">
    <div class="container">
        <div class="row">
            <div class="col-4 offset-2">
                <div class="card card-xl card-color-red">
                    <div class="card-header">
                        <h2 class="text-center text-color-purple"><?= __('accountant.login.page-title') ?></h2>
                    </div>
                    <div class="card-body">
                        <div>
                            Унесите корисничко име
                        </div>
                        <div class="padding-t-5">
                            <input type="text" class="t-input small f-username" autofocus="autofocus" placeholder="нпр. marko123">
                        </div>
                        <div class="padding-t-10 padding-b-10 w-100"></div>
                        <div>
                            Унесите лозинку
                        </div>
                        <div class="padding-t-5">
                            <input type="password" class="t-input small f-password" placeholder="Ваша лозинка">
                            <span class="text-faded text-size-12">Унесите лозинку са којом сте се регистровали</span>
                        </div>
                        <div class="margin-t-20 text-center">
                            <div class="t-button t-button-inline medium color-success login-button">
                                Пријавите се
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-6 padding-l-40 text-color-white">
                <div class="text-size-30 section">
                    Књиговођа
                </div>
                <h1 class="text-size-80">eCloud</h1>
                <div class="margin-t-20">
                    <h3 class="text-size-36 margin-t-10">Све на једном месту</h3>
                    <h4 class="text-size-16 margin-t-10">
                        Имате продавницу и желите да је повежете са књиговодством? Нема проблема!
                    </h4>
                </div>
            </div>

        </div>
    </div>
</div>