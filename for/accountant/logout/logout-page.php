<?php

if ( !defined( 'APP_REQ' ) )
    exit();

class LogoutPage extends \Core\Page {

    public function getTemplateUrl(): string
    {
        return __DIR__ . "/template.php";
    }

    public function init(): void
    {
        //echo "INIT";
    }

    public function canActivate(): bool
    {
        return false;
    }

    public function getTitleTranslationKey(): string
    {
        return "accountant.logout";
    }
}

// Create logout page
\Core\PageManager::assignCurrentPage(new LogoutPage());