<?php
    // Define AJAX environment.
    define( "IS_AJAX", false );

    include $_SERVER['DOCUMENT_ROOT'] . "/includes/application-start.php";
?>
<!doctype html>
<html lang="en">
<head>
    <?php ecloud_head(); ?>
    <title><?= __( \Core\PageManager::getPage()->getTitleTranslationKey() ) ?></title>
</head>
<body>

    <div class="page-content">

    </div>

    <div class="scripts-container">
        <?php // Include all scripts ?>
        <?php do_action('enqueue_scripts'); ?>
    </div>

    <div class="page-scripts-container"></div>

    <div class="loading-container">
        <div class="loading-content">
            <i class="fa fa-circle-notch fa-spin fa-2x"></i>
        </div>
    </div>
</body>
</html>