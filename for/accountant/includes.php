<?php

if ( !defined( 'APP_REQ' ) )
    exit();

// Set current section to accountant section & load all necessary files.
if ( IS_AJAX && str_contains(AJAX_REQUEST_TARGET, "accountant" ) ) {

    \Core\PageManager::setSection(\Core\PageManager::SECTION_ACCOUNTANT);

} elseif ( preg_match("/accountant/", $_SERVER['REQUEST_URI']) ) {

    \Core\PageManager::setSection(\Core\PageManager::SECTION_ACCOUNTANT);

};