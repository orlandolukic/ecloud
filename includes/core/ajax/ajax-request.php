<?php

namespace Core;

use JetBrains\PhpStorm\NoReturn;

if ( !defined('APP_REQ') )
    exit();

abstract class AjaxRequest {

    private array $data;
    protected array $requestData;

    public function __construct() {
        $this->requestData = array();
        $this->data = array(
            "requestData" => &$this->requestData
        );

        // Add other actions & perform filtering.
    }

    public abstract function handleRequest(): void;

    #[NoReturn] public function echoResult(): void {
        echo json_encode((object) $this->data);
        exit();
    }

}