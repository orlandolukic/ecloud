
core.managers.AsyncManager = function ($, core) {

    let AsyncRequest = function (method, url, data) {
        let m = method;
        let u = url;
        let d = data;
        let queueSuccess = new LinkedList();
        let queueError = new LinkedList();
        let queueBefore = new LinkedList();
        let x = {
            url: "/ajax" + url,
            method: method,
            cache: false,
            dataType: 'json',
            data: createDataForSubmission(data)
        }
        let promise = null;
        let y;

        y = {
            listenOnSuccess: function (callable) {
                queueSuccess.insert(callable);
                return y;
            },
            listenOnError: function (callable) {
                queueError.insert(callable);
                return y;
            },
            getPromise: function () {
                return promise;
            },
            before: function (callable) {
                queueBefore.insert(callable);
                return y;
            },
            initiate: function () {
                promise = new Promise((resolve, reject) => {
                    x.success = function (dta) {
                        queueSuccess.forEach((el) => {
                            el(dta);
                        });
                        resolve(dta);
                    };
                    x.error = function (err) {
                        queueError.forEach((el) => {
                            el(err);
                        });
                        reject(err);
                    };
                    queueBefore.forEach((el) => {
                        el(x);
                    });
                    $.ajax(x);
                });
                return promise;
            }
        }

        return y;
    }

    let createDataForSubmission;

    // Create AJAX request.
    let createRequest = function ( method, url, data ) {
        return new AsyncRequest(method, url, data);
    }

    createDataForSubmission = function (data) {
        let x = {};
        for (let key in data) {
            x[key] = data[key];
        }
        x.token = 0;
        x.userID = 0;
        return x;
    }

    return {
        POST: "POST",
        GET: "GET",
        createRequest: createRequest
    };
}