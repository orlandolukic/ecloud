
/**
 * Core module for message manipulation.
 *
 * @class MessageCenterManager
 * @memberOf managers
 * @namespace managers.MessageCenterManager
 * @access public
 */
core.managers.MessageCenterManager = function( $, core ) {

    // Include classes.
    let StackPane = core.classes.MessageCenterManager.StackPane;

    let manager;
    let panes;
    let size;

    let init = function () {

        size = $("body").width() - 100;
        size /= 3;

        panes = {
            top: {
                left: StackPane.__construct(1, "top-left", size),
                center: StackPane.__construct(1, "top-center", size),
                right: StackPane.__construct(1, "top-right", size),
            },
            bottom: {
                left: StackPane.__construct(-1, "bottom-left", size),
                center: StackPane.__construct(-1, "bottom-center", size),
                right: StackPane.__construct(-1, "bottom-right", size)
            },
        };
    }

    let prepareObject = function( obj, fieldArr ) {
        for (let i = 0; i<fieldArr.length; i++) {
            obj[fieldArr[i].name] = obj[fieldArr[i].name] !== undefined ? obj[fieldArr[i].name] : fieldArr[i].default;
        }
    }

    let pushMessage = function ( obj ) {

        if ( !obj )
            return;

        // Prepare object's data
        prepareObject(obj, [
            {
                default: null,
                name: 'position'
            },
            {
                name: 'onCreate',
                default: null
            },
            {
                name: 'text',
                default: "You forgot to put text into this message........."
            },
            {
                name: 'timeout',
                default: {}
            },
            {
                name: 'isSticky',
                default: false
            },
            {
                name: 'animation',
                default: 'zoom-in'
            }
        ]);
        prepareObject(obj.timeout, [
            {
                name: 'duration',
                default: 4000
            },
            {
                name: 'onStart',
                default: function() {}
            },
            {
                name: 'onPause',
                default: function() {}
            },
            {
                name: 'onContinue',
                default: function() {}
            }
        ]);

        if ( !obj.position )
            return;

        let position = obj.position.split(/-/g);
        if ( !position || position.length !== 2 )
            return;
        let vertical = position[0];
        let horizontal = position[1];
        try {
            let pane = panes[vertical][horizontal];
            return pane.pushMessage( obj );
        } catch( e ) {
            console.warn(e);
        }
        return null;
    };

    manager = {
        init: init,
        pushMessage: pushMessage
    };
    return manager;
}