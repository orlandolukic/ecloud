/**
 * Page Manager
 *
 * Handling requests about the page.
 */
core.managers.PageManager = function ($, core) {

    let pagesStack = new Stack();
    let AsyncManager;
    let HTMLDocumentManager;
    let controller = null;
    let available = true;

    let init = function () {
        pagesStack = new Stack();
        AsyncManager = core.getManager('AsyncManager');
        HTMLDocumentManager = core.getManager("HTMLDocumentManager");

        let pathname = location.pathname;
        pathname = pathname.replaceAll(/(^\/|\/$)/g, "");

        // Perform initial request, fetch current page.
        AsyncManager.createRequest(AsyncManager.POST, "/get-page", {
            pathname: pathname
        }).listenOnSuccess((payload) => {

            let request = payload.request;

            // Check received data
            if (request.redirect) {
                window.location.href = core.getDomain() + request.redirect;
                return;
            }
            document.title = request.page_title;

            // Put scripts on the page.
            HTMLDocumentManager.pushStyles( request.styles );
            HTMLDocumentManager.pushScripts( request.scripts );

            // Put content on the screen.
            $(".page-content").html(request.content);

            available = false;
            controller = core.getController(request.controller);
            if ( controller != null ) {
                controller.onPageOpen( request.payload );
            } else {
                console.error( "Controller '" + request.controller + "' is not registered within the system." );
            }
            available = true;

            setLoadingContainer(false);

        }).before((obj) => {
            setLoadingContainer(true);
        }).initiate();
    };

    let setLoadingContainer = function (val) {
        if (val) {
            $(".loading-container").show();
        } else {
            $(".loading-container").hide();
        }
    }

    return {
        init: init,
        setLoadingContainer: setLoadingContainer
    }
}
