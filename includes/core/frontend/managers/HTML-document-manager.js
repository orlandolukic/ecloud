/**
 * HTML Doducument Manager
 */
core.managers.HTMLDocumentManager = (function($, core) {

    let scriptsContainer;

    let init = function() {

        if ( core.isInitialized() )
            return;

        scriptsContainer = $(".page-scripts-container");
    };


    let pushScriptsToTheScreen = function ( arr ) {
        for (let i = 0; i<arr.length; i++)
            scriptsContainer.append(arr[i]);
    }

    return {
        init: init,
        pushStyles: pushScriptsToTheScreen,
        pushScripts: pushScriptsToTheScreen
    };
});