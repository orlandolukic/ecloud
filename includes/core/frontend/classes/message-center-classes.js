/**
 * @class
 */
core.classes.MessageCenterManager = {};

/**
 *
 */
core.classes.MessageCenterManager.Message = {

    // last ID of the created message
    ID: 0,

    __construct: function( obj ) {
        /**
         * @class Message
         * @memberOf MessageCenterManager
         */
        let instance;
        let id = ++this.ID;
        let timeout = null;
        let lastMouseIn = 0;
        let lastMouseOut = 0;
        let timeoutStart = 0;

        let messageBox;
        if ( obj.type && typeof obj.type !== "object" ) {
            messageBox = this.core.classes.MessageCenterManager.OrdinaryMessageBox.__construct(obj);
        } else if ( obj.type && typeof obj.type === "object"  ) {
            messageBox = obj.type.__construct(obj);
        } else {
            throw new Error("Could not create message box, type parameter is missing");
        }

        // Set timeout for this message
        if ( !obj.isSticky ) {
            let timeoutManipulation = {
                delay: 0,
                passed: 0,
                getHandler: function (callback) {
                    return () => {
                        // It's timeout, remove this message box.
                        messageBox.startDestroying();
                        setTimeout(() => {
                            messageBox.destroy();
                        }, 500);
                        callback();
                    };
                },
                resetTimeout: function () {
                    obj.timeout.onPause();
                    window.clearTimeout(timeout);
                    timeout = null;
                    lastMouseIn = new Date().getTime();
                },
                startTimeout: function (delay) {
                    this.delay = delay;
                    messageBox.getElement()
                        .addClass("animation-started")
                        .find(".loading-bar").css({
                        animationDuration: (obj.timeout.duration / 1000) + "s",
                        animationDelay: (delay / 1000) + "s"
                    });
                    timeout = window.setTimeout(this.getHandler(obj.timeout.onStart), obj.timeout.duration + delay);
                    timeoutStart = new Date().getTime();
                    lastMouseOut = timeoutStart;
                    this.passed = 0;
                },
                continueTimeout: function () {
                    this.passed += lastMouseIn - lastMouseOut;
                    let continueTime = (obj.timeout.duration + this.delay) - this.passed;
                    lastMouseOut = new Date().getTime();
                    timeout = window.setTimeout(this.getHandler(obj.timeout.onContinue), continueTime);
                }
            };

            // Set last mouse in.
            messageBox.getElement().on("mouseenter", function(e) {
                timeoutManipulation.resetTimeout();
            });
            messageBox.getElement().on("mouseleave", function(e) {
                timeoutManipulation.continueTimeout();
            });

            // Start timeout!
            timeoutManipulation.startTimeout( obj.animation ? 700 : 0 );
        } else {
            messageBox.getElement().find(".loading-bar").hide();
        }

        // Set on close.
        messageBox.getElement().find(".message-close-icon").on("click", function () {
            // It's timeout, remove this message box.
            messageBox.startDestroying();
            setTimeout(() => {
                messageBox.destroy();
            }, 500);
        });

        if ( typeof obj.onCreate === "function" )
            messageBox.onCreate(obj);
        else
            messageBox.writeDefault(obj);

        // ====================================================================================================
        //      Methods
        // ====================================================================================================

        let dismissMessage = function () {
            if ( timeout != null ) {
                window.clearTimeout(timeout);
                timeout = null;
            };
            messageBox.destroy();
        };

        instance = {
            getElement: function () { return messageBox.getElement(); },
            getID: function() { return id; },
            dismissMessage: dismissMessage
        };
        return instance;
    }
};

/**
 *
 * @type {{__construct: (function(*): {onCreate: onCreate, startDestroying: startDestroying, writeDefault: writeDefault, getElement: (function(): *), destroy: destory})}}
 * @return {{
 *     a: int,
 *     b: string,
 *     c: Message,
 *     getString: function(a,b,c)
 * }} "luka"
 */
core.classes.MessageCenterManager.OrdinaryMessageBox = {

    __construct: function( obj ) {
        let instance;
        let icon;
        let size;

        switch (obj.type) {
            case "danger":
                icon = "fas fa-exclamation-triangle";
                size = 3;
                break;
            case "info":
                icon = "fas fa-info-circle";
                size = 3;
                break;
            case "warning":
                icon = "fas fa-exclamation-triangle";
                size = 3;
                break;
            case "success":
                icon = "fas fa-check-circle";
                size = 3;
                break;
        }

        let box = this.jquery("<div></div>");
        box.addClass('single-message').addClass("ordinary-message").addClass("background-color-pure-white");
        box.addClass( "message-type-" + obj.type );
        box.append(
            '<div class="message-icon">' +
                '<i class="' + icon + ' fa-' + size + 'x"></i>' +
            '</div>' +
            '<div class="message-content">' +
                '<div class="message-title">' +
                    '<h3></h3>' +
                '</div>' +
                '<div class="message-text">' +
                    '' +
                '</div>' +
            '</div>' +
            '<div class="message-close-icon">' +
                '<i class="fas fa-times-circle"></i>' +
            '</div>' +
            '<div class="loading-bar"></div>'
        );
        if ( obj.animation )
            box.addClass(obj.animation);

        let writeDefault = function ( obj ) {
            box.find(".message-content .message-text").html( obj.text );
            box.find(".message-content .message-title").html( obj.title );
        }

        /**
         * Manipulation with text inside the ordinary message box.
         * @see {@link Message}
         *
         * @package MessageCenter
         * @class ManipulationObject
         */
        let createManipulationObject = function () {
            return {
                getTitleArea: function() {
                    let wrappingAround = "h3";
                    let titleElem = box.find(".message-content .message-title");
                    let paddingClass = null;
                    let marginClass = null;
                    let i = {
                        getElement: function () {
                            return titleElem;
                        },
                        wrapAround: function (w) { wrappingAround = w; return i; },
                        write: function (text) {
                            titleElem.html(
                                "<" + wrappingAround + ">" + text + "</" + wrappingAround + ">"
                            );
                            return i;
                        },
                        addPadding: function ( from, value ) {
                            if ( from !== "top" && from !== "left" && from !== "bottom" && from !== "right" && value % 2 !== 0 )
                                return;
                            if ( paddingClass !== null )
                                titleElem.removeClass(paddingClass);
                            let p = "padding-" + from.charAt(0) + "-" + value;
                            paddingClass = p;
                            titleElem.addClass(p);
                            return i;
                        },
                        addMargin: function ( from, value ) {
                            if ( from !== "top" && from !== "left" && from !== "bottom" && from !== "right" && value % 2 !== 0 )
                                return;
                            if ( marginClass !== null )
                                titleElem.removeClass(marginClass);
                            let p = "margin-" + from.charAt(0) + "-" + value;
                            marginClass = p;
                            titleElem.addClass(p);
                            return i;
                        }
                    }
                    return i;
                },
                getTextArea: function() {
                    let textElem = box.find(".message-content .message-text");
                    let paddingClass = null;
                    let marginClass = null;
                    let i = {
                        getElement: function () {
                            return textElem;
                        },
                        write: function (text) {
                            textElem.html(text);
                            return i;
                        },
                        addPadding: function ( from, value ) {
                            if ( from !== "top" && from !== "left" && from !== "bottom" && from !== "right" && value % 2 !== 0 )
                                return;
                            if ( paddingClass !== null )
                                textElem.removeClass(paddingClass);
                            let p = "padding-" + from.charAt(0) + "-" + value;
                            paddingClass = p;
                            textElem.addClass(p);
                            return i;
                        },
                        addMargin: function ( from, value ) {
                            if ( from !== "top" && from !== "left" && from !== "bottom" && from !== "right" && value % 2 !== 0 )
                                return;
                            if ( marginClass !== null )
                                textElem.removeClass(marginClass);
                            let p = "margin-" + from.charAt(0) + "-" + value;
                            marginClass = p;
                            textElem.addClass(p);
                            return i;
                        }
                    }
                    return i;
                },
                getMessageBox: function () {
                    return box;
                }
            };
        }

        let onCreate = function (obj) {
            obj.onCreate( createManipulationObject() );
        }

        let destory = function () {
            box.remove();
            box = null;
        }

        let startDestroying = function () {
             box.addClass("auto-destruct");
        };

        instance = {
            getElement: function () { return box; },
            writeDefault: writeDefault,
            onCreate: onCreate,
            destroy: destory,
            startDestroying: startDestroying
        }
        return instance;
    }
};

/**
 *
 * @type {{__construct: (function(*, *, *): {pushMessage: (function(*=): number)})}}
 */
core.classes.MessageCenterManager.StackPane = {

    /**
     * Creates one StackPane.
     *
     * @param dir
     * @param c Position classname
     * @param s
     * @returns {{}}
     */
    __construct: function( dir, c, s ) {
        let pane;
        let classname = c;
        let direction = dir;
        let container;
        let size = s;
        let that = this;
        let isCreated = false;

        // Create container for this stack pane.
        let createContainer = function() {
            container = that.jquery('<div></div>');
            container.addClass("message-stack-pane").addClass(classname);
            container.css({
                maxWidth: size + "px"
            });
            that.jquery("body").append(container);
        }

        let pushMessage = function ( obj ) {

            // Create message.
            let message = that.core.classes.MessageCenterManager.Message.__construct(obj);

            // Create container if necessary.
            if ( !isCreated ) {
                createContainer();
                isCreated = true;
            }

            if ( direction < 0 )
                container.prepend(message.getElement());
            else
                container.append(message.getElement());

            return message;
        };

        pane = {
            pushMessage: pushMessage
        };
        return pane;
    }
};