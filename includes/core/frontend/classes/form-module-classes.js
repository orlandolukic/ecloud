core.classes.FormModule = {};

core.classes.FormModule.ButtonGroup = {
    jquery: null,
    core: null,
    __construct: function (n) {
        let name = n;
        let buttons = new LinkedList();
        let that = this;
        let instance;
        let addButton = function (btn) {
            if ( typeof btn === typeof "" )
                btn = that.core.modules.ButtonModule(that.jquery, that.core, btn);
            buttons.insert(btn);
            return btn;
        };

        instance = {
            addButton: addButton,
            forEach: function( callback ) { buttons.forEach((btn) => { callback(btn); }); },
            getButton: function( name ) {
                let x = { btn: null }
                buttons.forEach((btn) => {
                    if ( btn.equals(name) )
                        x.btn = btn;
                });
                return x.btn;
            },
            setLoading: function( value ) { buttons.forEach((btn) => { btn.setLoading(value); }); return instance; },
            setEnabled: function( value ) { buttons.forEach((btn) => { btn.setEnabled(value); }); return instance; },
            getName: function () { return name; },
            equals: function( n ) {
                return n === name;
            }
        };

        return instance;
    }
};

core.classes.FormModule.ActionButton = {
    jquery: null,
    core: null,
    __construct: function ( c, prm ) {
        let button = core.modules.ButtonModule(this.jquery, this.core, c);
        let isPrimary = prm;
        return {
            getButton: function () { return button; },
            isPrimary: function () { return isPrimary; },
            setPrimary: function( val ) { isPrimary = val; },
            equals: function( classname ) {
                return button.getClassName() === classname;
            }
        };
    }
};

// Field class.
core.classes.FormModule.Field = {
    jquery: 25,
    core: null,
    __construct: function( i1, i2 ) {
        let id = i1;
        let classname = i2;
        let element = this.jquery("." + classname);
        return {
            getId: function () { return id; },
            getClassName: function () { return classname; },
            getElement: function () { return element; },
            getValueOfTheElement: function () { return element.val(); },
            equals: function( fid ) {
                return fid === id;
            }
        }
    }
};
