/**
 * Core module for button manipulation.
 *
 * @param $
 * @param core
 * @param c
 * @constructor
 */
core.modules.ButtonModule = function( $, core, c ) {

    let container = $(".page-content " + c);
    let classname = c;
    let module;
    let disabled = false;

    if ( container.length > 0 ) {

        // Initialize the button.
        let content = container.html();
        content = content.replaceAll(/(\s|\t|\r\n){2,}/g, "");
        container.html(
            '<div class="t-button-loading">' +
                '<div class="loading-container-content"></div>' +
            '</div>' +
            '<div class="t-button-content">' +
                content +
            '</div>'
        );

    } else {
        console.error("Could not make button module for the given target");
        return null;
    }

    /**
     * A
     * @param eventName
     * @param callback
     */
    let setAction = function( eventName, callback ) {
        $(container).on(eventName, function (e, data) {
            if ( disabled )
                return;
            console.log("HERE");
            e.data = data;
           callback(e);
        });
        return module;
    };

    let setLoading = function ( working ) {
        disabled = true;
        if ( working )
            container.addClass("is-loading");
        else
            container.removeClass("is-loading");
    }

    triggerEvent = function ( eventName, data ) {
        $(container).trigger(eventName, data);
    }

    // Destroy button.
    let destroy = function () {
        $(container).off();
        container = null;
        module = null;
    };

    let setEnabled = function ( value ) {
        disabled = !value;
        if ( value )
            container.removeClass("is-disabled");
        else
            container.addClass("is-disabled");
    }

    let getClassName = function () { return classname; };

    let equals = function ( c ) {
        return c === classname;
    }

    module = {
        setAction: setAction,
        setLoading: setLoading,
        setEnabled: setEnabled,
        getClassName: getClassName,
        triggerEvent: triggerEvent,
        destroy: destroy,
        equals: equals
    };

    return module;
}