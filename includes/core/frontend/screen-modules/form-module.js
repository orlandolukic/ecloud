
/**
 * Core module for form manipulation.
 * Form submissions, field settings.
 *
 * @param $
 * @param core
 * @param c
 * @constructor
 */
core.modules.FormModule = function( $, core ) {

    let ActionButton = core.classes.FormModule.ActionButton;
    let ButtonGroup = core.classes.FormModule.ButtonGroup;
    let Field = core.classes.FormModule.Field;

    // ============================================================================================
    //      Fields
    // ============================================================================================

    let fields = new LinkedList();
    let actionButtons = {};
    let currentAction = null;
    let buttonGroups = new LinkedList();
    let module;
    let inSubmission;
    let onSubmitEvent = {
        onSubmitHandler: null,
        onBeforeSubmitHandler: null
    };

    // ============================================================================================
    //      Functions
    // ============================================================================================

    let getValueFor = function ( fieldName ) {
        let f = fields.getContent(fieldName).getValueOfTheElement();
        console.log(f);
    }

    let enable = function ( val ) {
        fields.forEach((field) => {
            field.getElement().prop('disabled', !val);
        });
    }

    // Form submission
    let submitForm = function () {

        if ( onSubmitEvent.onBeforeSubmitHandler )
            onSubmitEvent.onBeforeSubmitHandler(module);

        inSubmission = true;
        enable(false);

        if ( onSubmitEvent.onSubmitHandler )
            onSubmitEvent.onSubmitHandler(module);
    };

    let isInSubmission = function () {
        return inSubmission;
    }

    let addField = function ( id, classname ) {
        let f = Field.__construct(id, classname);
        fields.insert(f);
    }

    // Initializes form with enter section
    let init = function ( initData ) {

        // Copy handlers!
        onSubmitEvent.onSubmitHandler = initData.onSubmit;
        onSubmitEvent.onBeforeSubmitHandler = initData.onBeforeSubmit;

        let x = {
            cnt: 0,
            previous: null,
            size: fields.size()
        };
        fields.forEach((field) => {
            // Enable enter functionality
            if ( initData.enableEnter ) {
                if (x.cnt > 0) {
                    x.previous.getElement().on("keyup", function (e) {
                        if (e.keyCode === 13)
                            field.getElement().focus();
                    })
                }
            }

            // Enable on submit last.
            if ( initData.onLastSubmit ) {
                if (x.cnt === x.size-1) {
                    field.getElement().on("keyup", function (e) {
                        if (e.keyCode === 13) {
                            submitForm();
                        }
                    })
                }
            }

            x.previous = field;
            x.cnt++;
        });


    }

    let addActionButton = function ( actionName, classname, isPrimary ) {
        actionButtons[actionName] = new ActionButton(classname, isPrimary);
    };

    let getActionButton = function ( actionName ) {
        return actionButtons[actionName] ? actionButtons[actionName] : null;
    }

    // Destroy this form.
    let destroy = function () {

        // Destroy each button.
        for (let key in actionButtons) {
            actionButtons[key].getButton().destroy();
        }

        // Destroy everything connected to the fields.
        fields.forEach((field) => {
            field.off("keyup");
        });
    };

    let setCurrentAction = function ( actionName ) {
        currentAction = actionName;
    };

    // =====================================================
    //               Button groups manipulation
    // =====================================================

    let getButtonGroup = function (name) {
        let x = {
            group: null
        };
        buttonGroups.forEach((btnGroup) => {
            if ( btnGroup.equals(name) ) {
                x.group = btnGroup;
            }
        });
        return x.group;
    };

    let createButtonGroup = function (name) {

        if ( buttonGroups.contains(name) )
            return;

        let b = ButtonGroup.__construct(name);
        buttonGroups.insert(b);

        return b;
    };

    let addButtonIntoGroup = function (groupName, classname) {
        let group = getButtonGroup(groupName);
        if ( !group )
            return;
        group.addButton( core.modules.ButtonModule($, core, classname) );
        return module;
    }

    let forEachButtonInTheGroup = function ( groupName, callback ) {
        let group = getButtonGroup(groupName);
        if ( !group )
            return;
        group.forEach(callback);
        return module;
    }

    let getButtonFromGroup = function ( name ) {

        let x = {
            btn: null
        };
        buttonGroups.forEach((group) => {
            group.forEach((btn) => {
                if ( btn.equals(name) ) {
                    x.btn = btn;
                }
            });
        });

        return x.btn;
    }

    module = {
        addField: addField,
        addActionButton: addActionButton,
        getActionButton: getActionButton,
        setCurrentAction: setCurrentAction,

        init: init,
        destroy: destroy,
        submit: submitForm,
        isInSubmission: isInSubmission,
        enableFields: enable,
        getValueFor: getValueFor,

        // Button groups manipulation
        createButtonGroup: createButtonGroup,
        getButtonGroup: getButtonGroup,
        addButtonIntoGroup: addButtonIntoGroup,
        forEachButtonInTheGroup: forEachButtonInTheGroup,
        getButtonFromGroup: getButtonFromGroup
    };

    return module;
}