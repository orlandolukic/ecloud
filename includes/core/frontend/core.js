/**
 * System core
 */
core = (function($){

    let systemVariables = {
        initialized: false
    };
    let managers = {};
    let controllers = {};

    let operations = {
        modules: {},
        classes: {},
        managers: {},
        getDomain: function () { return "https://ecloud.rs"; },
        getAssetsDir: function() { return this.getDomain() + "/assets" },
        getManager: function(name) {
            return managers[name];
        },
        createController: function () {
          return controllers["Controller"]();
        },
        getController: function(name) {
            return controllers[name];
        },
        isInitialized: function () { return systemVariables.initialized; }
    };

    // Init all parts of the system.
    let assign = function () {

        // Assign classes!
        operations.classes = core.classes;
        for (let moduleName in core.classes) {
            for (let cls in operations.classes[moduleName]) {
                // Assign all important variables to this classes.
                operations.classes[moduleName][cls].jquery = $;
                operations.classes[moduleName][cls].core = operations;
            }
        }

        let obj = core.managers;
        for(let key in obj) {
            if ( key === "start" )
                continue;
            operations.managers[key] = managers[key] = obj[key]($, operations);
            delete obj[key];
        }

        obj = core.controllers;
        for(let key in obj) {
            controllers[key] = obj[key]($, operations);
            delete obj[key];
        }

        obj = core.modules;
        for(let key in obj) {
            operations.modules[key] = obj[key];
            delete obj[key];
        }
    }

    // Initializes the system.
    let init = function() {
        for ( let key in managers ) {
            if ( managers[key].init !== undefined )
                managers[key].init();
        }
    };

    let afterInit = function () {
        delete core.managers;
        delete core.controllers;
        delete core.modules;
        delete core.classes;

        for ( let key in managers ) {
            if ( managers[key].afterInit !== undefined )
                managers[key].afterInit();
        }
    }

    // Set restrictions for the system.
    let restrict = function() {
        window.onselectstart = function(e) {
            e.preventDefault();
        }
    }

    return {
        start: function() {
            if ( !systemVariables.initialized ) {

                // Add systems restrictions.
                restrict();

                // Assign all variables to the system.
                assign();

                // Initialize the system.
                init();

                // Execute actions after initialization.
                afterInit();

                systemVariables.initialized = true;
            }
        },
        managers: {},
        controllers: {},
        modules: {},
        classes: {}
    }
})(jQuery);

/**
 * Base controller class.
 * @class
 */
core.controllers.Controller = function ($, core) {
    return function () {
        return {
            onPageOpen: function () {},
            onBeforePageDeactivation: function() {
                return true
            },
            onPageClose: function () {}
        };
    }
}

$(document).ready(function (e) {
    core.start();
    /**
     * @class
     */
    core.classes = {};
    /**
     * @class
     */
    core.managers = {};

    /**
     * @class
     */
    core.controllers = {};

    /**
     * @class
     */
    core.modules = {};
    delete core.start;
});
