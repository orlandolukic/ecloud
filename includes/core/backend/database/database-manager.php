<?php

namespace Core\Database;

use PDO;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class DatabaseManagerListElement {

    private DatabaseConnection $connection;
    private ?DatabaseManagerListElement $next;

    public function __construct( DatabaseConnection $connection )
    {
        $this->connection = $connection;
        $this->next = null;
    }

    /**
     * @return DatabaseConnection
     */
    public function getDatabaseConnection(): DatabaseConnection
    {
        return $this->connection;
    }

    /**
     * @return DatabaseManagerListElement|null
     */
    public function getNext(): ?DatabaseManagerListElement
    {
        return $this->next;
    }

    /**
     * @param DatabaseManagerListElement|null $next
     */
    public function setNext(?DatabaseManagerListElement $next): void
    {
        $this->next = $next;
    }

    public function equals( string $dbname, string $username, string $password ): bool {
        if ( $connection == null )
            return false;
        return $this->connection->equals($dbname, $username, $password);
    }

}

class DatabaseManager {

    private static ?DatabaseManager $manager =  null;
    public static function get(): DatabaseManager {
        if ( self::$manager == null )
            self::$manager = new DatabaseManager();
        return self::$manager;
    }

    private ?DatabaseManagerListElement $first;
    private ?DatabaseManagerListElement $last;
    private int $databases;

    public function __construct()
    {
        $this->first = null;
        $this->last = null;
        $this->databases = 0;
        add_action('core_connect_databases', 10, array( $this, 'connectWithDatabase' ) );
        add_action( 'core_disconnect_databases', 10, array( $this, 'disconnectAllDatabases' ) );
    }

    public function getDatabase( string $name ): ?DatabaseConnection {
        $current = $this->first;
        while( $current != null ) {
            if ( !strcmp( $name, $current->getDatabaseConnection()->getDatabaseName() ) ) {
                return $current->getDatabaseConnection();
            }
            $current = $current->getNext();
        }
        return null;
    }

    public function getDatabaseConnection( string $name ): ?PDO {
        $current = $this->first;
        while( $current != null ) {
            if ( !strcmp( $name, $current->getDatabaseConnection()->getDatabaseName() ) ) {
                return $current->getDatabaseConnection()->getConnection();
            }
            $current = $current->getNext();
        }
        return null;
    }

    public function makeConnection( string $host, string $dbname, string $username, string $password ): void {
        if ( $this->_checkIfExists($dbname, $username, $password) )
            return;

        // Create database connection and it to the list.
        $dbconn = new DatabaseConnection($host, $dbname, $username, $password);
        $element = new DatabaseManagerListElement($dbconn);
        if ( $this->first == null ) {
            $this->first = $this->last = $element;
        } else {
            $this->last->setNext( $element );
            $this->last = $element;
        }
        $this->databases++;
    }

    public function disconnectAllDatabases(): void {
        $current = $this->first;
        // Disconnect all database connections.
        while( $current != null ) {
            $current->getDatabaseConnection()->disconnect();
            $current = $current->getNext();
        }
    }

    public function connectWithDatabase(): void {
        $current = $this->first;
        while( $current != null ) {

            // Try to connect with the database.
            $current->getDatabaseConnection()->connect();

            // Execute every action action associated with this database connection.
            do_action( "core_database_" . $current->getDatabaseConnection()->getDatabaseName() . "_connected",
                $current->getDatabaseConnection()->getConnection() );

            $current = $current->getNext();
        }
    }

    #[Pure] private function _checkIfExists( string $dbname, string $username, string $password ): bool {
        $current = $this->first;
        while( $current != null ) {
            if ( $current->getDatabaseConnection()->equals($dbname, $username, $password) ) {
                return true;
            }
            $current = $current->getNext();
        }
        return false;
    }

    #[Pure] public function getDatabaseCount(): int {
        return $this->databases;
    }

}