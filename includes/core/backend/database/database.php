<?php

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

require 'database-connection.php';
require 'database-manager.php';
require 'query/query.php';
require 'query/select-query.php';
require 'query/where-clause.php';
require 'query/order-clause.php';
require 'query/aggregate-functions/aggregate-function.php';
require 'query/aggregate-functions/max-aggregate-function.php';
require 'query/aggregate-functions/min-aggregate-function.php';
require 'query/aggregate-functions/avg-aggregate-function.php';
require 'query/aggregate-functions/sum-aggregate-function.php';
require 'query/aggregate-functions/count-aggregate-function.php';
require 'query/helper-functions.php';
require 'query/where-part.php';
require 'table-names/general-table-names.php';

function add_database( string $host, string $dbname, string $username, string $password ) {
    \Core\Database\DatabaseManager::get()->makeConnection($host, $dbname, $username, $password);
}

function get_database( string $name ): ?\Core\Database\DatabaseConnection {
    return \Core\Database\DatabaseManager::get()->getDatabase($name);
}

// Define all necessary constants
add_action( "define_constants", 10, array( "\Core\Database\Query", "define_constants" ) );
