<?php

namespace Core\Database;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

function get_table_with_field( string $field ): ?array {

    if ( !str_starts_with($field, "field::") )
        return null;

    $a = array();
    $v = str_replace("field::", "", $field);
    $g = preg_split( "/\./", $v );
    if ( $g && count($g) == 2 ) {
        $a[0] = $g[0];
        $a[1] = $g[1];
    } else
        return null;

    return $a;
}

function get_full_field( array &$arr ): string {
    return "`$arr[0]`.`$arr[1]`";
}