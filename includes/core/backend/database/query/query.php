<?php

namespace Core\Database;

// Silence is golden!
use PDO;

if ( !defined("APP_REQ") )
    exit();

abstract class Query {

    /**
     * Defines all important constants for system to run!
     */
    public static function define_constants(): void {
        define("PERFORM_QUERY_PARAMETERS_CHECK", true);
    }

    protected string $databaseName;
    protected ?PDO $connection;
    protected ?string $queryString;

    /**
     * Creates query with parameters
     * @param string|null $dbname
     */
    public function __construct( string $dbname = null )
    {
        $this->queryString = null;
        $this->databaseName = !$dbname ? 'ecloud': $dbname;
        $this->connection = null;
    }

    /**
     * Prepares query string.
     *
     * @param array $properties
     * @throws \Exception if parameters are not set properly
     */
    protected abstract function _prepareQueryString(): void;

    /**
     * Checks query parameters
     */
    protected function checkQueryParameters(): void {}

    /**
     * Creates metadata for this query.
     */
    protected function createMetadata(): void {}

    public function getQueryString(): ?string {

        // Check query parameters if needed.
        if ( PERFORM_QUERY_PARAMETERS_CHECK ) {
            $this->checkQueryParameters();
        };

        $this->_prepareQueryString();
        return $this->queryString;
    }

    protected function getFullTableName( string $tbl ): string {
        return '`' . $this->databaseName . '`.`' . $tbl . '`';
    }

    /**
     * @param string|null $queryString
     * @return Query
     */
    public function setQueryString(string $queryString): Query
    {
        $this->queryString = $queryString;
        return $this;
    }

    /**
     * Executes query.
     * @param array|null $params
     * @return array|null
     * @throws \Exception
     */
    public function &execute(array $params = array()): array|null {
        if ( !$this->connection ) {
            $c = DatabaseManager::get()->getDatabaseConnection($this->databaseName);
            if ($c)
                $this->connection = $c;
            else
                throw new \Exception("Connection to database '" . $this->databaseName . "' is not established");
        };
        $ret = $this->_execute($params);
        return $ret;
    }

    protected abstract function _execute(array $params): array|null;
}