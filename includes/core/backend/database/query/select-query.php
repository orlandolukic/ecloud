<?php

namespace Core\Database;

// Silence is golden!
use Closure;
use PDO;

if ( !defined("APP_REQ") )
    exit();

class FieldName {
    private string $name;
    private string $fullname;
}

class SelectQuery extends Query {

    public const PART_FIELDS = 1;
    public const PART_WHERE = 2;
    public const PART_GROUPBY = 3;
    public const PART_HAVING = 4;
    public const PART_ORDERBY = 5;

    private static function _createQueryString(SelectQuery $query, string &$text ): void {
        $text .= "SELECT";

        // Create fields part.
        self::_queryFields($query, $text);

        $text .= "FROM";

        // Create from part.
        self::_queryFrom($query, $text);

        // Create 'join' part.
        self::_queryJoin($query, $text);

        // Create from part.
        self::_queryWhere($query, $text);

        // Create 'group by' part of the query.
        self::_queryGroup($query, $text);

        // Create 'having' part of the query.
        self::_queryHaving($query, $text);

        // Create 'order by' part.
        self::_queryOrderby($query, $text);

        // Create limit & offset part.
        self::_queryLimitOffset( $query, $text );
    }

    private static function _queryLimitOffset( SelectQuery $query, string &$text ): void {
        if ( $query->limit > 0 ) {
            $text .= " LIMIT " . $query->limit;
        }
        if ( $query->offset > 0 )
            $text .= " OFFSET " . $query->offset;
    }

    private static function _queryFields( SelectQuery $query, string &$text ): void {
        $text .= " ";
        $has = false;
        if ( $query->fields ) {
            $fields = $query->fields;
            $n = count($fields);
            $has = count($fields) > 0;
            $text .= " ";
            for ( $i=0; $i<$n; $i++ ) {
                if ( is_array( $fields[$i] ) ) {

                    if ( isset( $fields[$i]["distinct"] ) && $fields[$i]["distinct"] )
                        $text .= " DISTINCT ";

                    if ( isset( $fields[$i]["query"] ) && $fields[$i]["query"] instanceof SelectQuery) {
                        $text .= " (";
                        $fields[$i]["query"]->_prepareQueryString();
                        $text .= $fields[$i]["query"]->getQueryString();
                        $text .= ")";
                    } else {
                        $tbl = isset( $fields[$i]["table"] ) ? $fields[$i]["table"] : null;
                        $text .= $query->getFullField( $fields[$i]["name"], self::PART_FIELDS, $tbl);
                    };
                    if ( isset( $fields[$i]["alias"] ) )
                        $text .= " AS " . $fields[$i]["alias"];
                } else
                    $text .= $query->getFullField( $fields[$i] );

                // Add ','
                if ( $i+1<$n ) {
                    $text .= ", ";
                }
            }
            $text .= " ";
        } else {
            $text .= self::_getTableName($query) . ".*";
            $has = true;
        }

        // Process aggregate functions if present.
        if ( isset( $query->group["fields"] ) ) {
            if ( $has )
                $text .= ", ";
            if ( is_array( $query->group["fields"] ) ) {
                $n = count($query->group["fields"]);
                for ($i = 0; $i<$n; $i++) {
                    if ( $query->group["fields"][$i] instanceof AggregateFunction ) {
                        $text .= $query->group["fields"][$i]->printContent();
                    };
                    if ( $i + 1 < $n )
                        $text .= ", ";
                }
            } else if ( $query->group["fields"] instanceof AggregateFunction ) {
                $text .= $query->group["fields"]->printContent();
            };
        }

        $text .= " ";
    }

    private static function _queryJoin(SelectQuery $query, string &$text)
    {
        if ( $query->join ) {
            for ($i = 0; $i < count( $query->join ); $i++ ) {
                $text .= " ";
                $text .= match ($query->join[$i]["type"]) {
                    "left" => "LEFT OUTER JOIN",
                    "right" => "RIGHT OUTER JOIN",
                    "inner" => "INNER JOIN"
                };

                $text .= " ";

                $text .= "`" . $query->join[$i]["table"] . "`";
                if ( isset( $query->join[$i]["alias"] ) )
                    $text .= " " . $query->join[$i]["alias"];

                $text .= " ON ";

                $text .= $query->join[$i]["condition"]->printWherePart();
            }
        }
    }

    private static function _queryGroup( SelectQuery $query, string &$text ): void {
        if ( $query->group && is_array( $query->group ) ) {
            $text .= " GROUP BY ";
            if ( $query->group["by"] && is_array( $query->group["by"] ) ) {
                $n = count($query->group["by"]);
                for ($i = 0; $i < $n; $i++) {
                    $text .= $query->getFullField($query->group["by"][$i], SelectQuery::PART_GROUPBY);
                    if ($i + 1 < $n)
                        $text .= ", ";
                }
            } else if ( $query->group["by"] && is_string($query->group["by"]) ) {
                $text .= $query->getFullField($query->group["by"], self::PART_GROUPBY);
            };
        };
    }

    private static function _queryHaving( SelectQuery $query, string &$text ): void {
        if ( $query->having && $query->having instanceof WhereClause ) {
            $text .= " HAVING ";
            $text .= $query->having->printWherePart();
        };
    }

    private static function _queryWhere(SelectQuery $query, string &$text)
    {
        if ( $query->condition && $query->condition instanceof WhereClause) {
            $text .= " WHERE ";
            $text .= $query->condition->printWherePart();
        };
    }

    private static function _queryOrderby( SelectQuery $query, string &$text ): void {
        if ( $query->order && $query->order instanceof OrderByClause)
            $text .= $query->order->printOrderClause();
    }

    private static function _getTableName( SelectQuery|array $p ): string {
        if ( is_array( $p ) ) {
            $t = "`" . $p["name"] . "`";
            return $t;
        } else {
            if (is_string($p->table)) {
                return '`' . $p . '`';
            } else if (is_array($p->table)) {
                $text = "UNDEFINED";
                if (isset($p->table["name"])) {
                    $text = "`" . $p->table["name"] . "`";
                } else if (isset($p->table["alias"]))
                    $text = "`" . $p->table["alias"] . "`";
                return $text;
            } else {
                return "`" . $p->table["alias"] . "`";
            }
        };
    }

    private static function _queryFrom( SelectQuery $query, string &$text ): void {
        $text .= " ";
        if ( is_string( $query->table ) )
            $text .= "`" . $query->table . "`";
        else if ( is_array( $query->table ) ) {

            if ( isset( $query->table["query"] ) && $query->table["query"] instanceof SelectQuery) {
                $text .= " (";
                // Select query.
                self::_createQueryString( $query->table["query"], $text );
                $text .= ")";
            } else
                $text .= "`" . $query->table["name"] . "`";

            if ( isset( $query->table["alias"] ) )
                $text .= " " . $query->table["alias"];

        };
    }

    // ============================================================================================================

    private string|array|SelectQuery|null $table;
    private ?array $fields;
    private array $tablesWithFields;
    private array $tableFields;
    private ?array $join;
    private ?WhereClause $condition;
    private ?OrderByClause $order;
    private ?WhereClause $having;
    private ?array $group;
    private array $aggregateFields;
    private int $limit;
    private int $offset;
    private bool $isCreatedMetadata;
    private bool $isNested;

    public function __construct( array $parameters, string $dbname = null )
    {
        parent::__construct( $dbname );
        $this->table = isset( $parameters["table"] ) ? $parameters["table"] : null;
        $this->fields = isset( $parameters["fields"] ) ? $parameters["fields"] : null;
        $this->join = isset( $parameters["join"] ) ? $parameters["join"] : null;
        if ( $this->join ) {
            for ($i = 0; $i<count( $this->join ); $i++) {
                $this->join[$i]["condition"]->assignSelectQuery($this);
            }
        };
        $this->group = isset( $parameters["group"] ) ? $parameters["group"] : null;
        $this->condition = isset( $parameters["condition"] ) ? $parameters["condition"] : null;
        if ( $this->condition && $this->condition instanceof WhereClause )
            $this->condition->assignSelectQuery($this);
        $this->order = isset( $parameters["order"] ) ? $parameters["order"] : null;
        if ( $this->order && $this->order instanceof OrderByClause )
            $this->order->assignSelectQuery($this);

        $this->having = isset( $parameters["having"] ) ? $parameters["having"] : null;
        if ( $this->having && $this->having instanceof WhereClause )
            $this->having->assignSelectQuery($this);

        $this->limit = isset( $parameters["limit"] ) ? $parameters["limit"] : 0;
        $this->offset = isset( $parameters["offset"] ) ? $parameters["offset"] : 0;
        $this->tablesWithFields = array();
        $this->isCreatedMetadata = false;
        $this->isNested = false;
        $this->tableFields = array();
        $this->aggregateFields = array();
    }

    protected function _prepareQueryString(): void
    {
        if ( $this->queryString )
            return;

        // Get metadata.
        $this->createMetadata();

        $this->queryString = "";
        self::_createQueryString( $this, $this->queryString );
    }

    protected function createMetadata(): void
    {
        if ( $this->isCreatedMetadata )
            return;

        // Add current table.
        //$this->_addTable('this', 'this', array(), false);

        // Add main table.
        // Check table
        if ( is_string( $this->table ) ) {
            $this->_addTable($this->table, null, array(), false);
        } else if ( is_array( $this->table ) ) {
            $alias = null;
            $fields = array();
            $is_nested = false;
            if ( isset( $this->table["query"] ) && $this->table["query"] instanceof SelectQuery ) {
                $name = $this->table["alias"];
                $alias = $this->table["alias"];

                // Add all fields to this query.
                $this->table["query"]->setNested(true);
                $this->table["query"]->createMetadata();
                $fields = $this->table["query"]->getAllTableFields();
                $is_nested = true;
            } else {
                $name = $this->table["name"];
                $alias = isset( $this->table["alias"] ) ? $this->table["alias"] : null;
            };
            $this->_addTable($name, $alias, $fields, $is_nested);
            //echo "Here $name $alias<br>";
        }

        // Prepare join parameter.
        if ( $this->join ) {
            for ($i = 0; $i < count($this->join); $i++) {
                $alias = $this->join[$i]["table"];
                if ( isset( $this->join[$i]["alias"] ) ) {
                    $alias = $this->join[$i]["alias"];
                }
                $this->_addNewTable( $this->join[$i]["table"], $alias, false );
                if ( $this->join[$i]["fields"] && is_array( $this->join[$i]["fields"] )
                ) {
                    for ($j = 0; $j < count($this->join[$i]["fields"]); $j++) {
                        $this->addFieldIntoTable( $this->join[$i]["table"], $this->join[$i]["fields"][$j] );
                    }
                };
            }
        };

        // Set part having.
        if ( $this->having && $this->having instanceof WhereClause ) {
            $this->having->setPart(self::PART_HAVING);
        }

        // Put fields in selected table.
        $x = &$this->fields;
        if ( is_array($x) ) {
            for ($i = 0; $i < count($x); $i++) {
                if (is_string($x[$i])) {

                    if ($this->fieldExists($x[$i]))
                        continue;
                    else {
                        if ($this->isNested)
                            throw new \Exception("Field '" . $x[$i] . "' does not exist");
                    };

                    $this->tablesWithFields[0]["fields"][] = array(
                        $x[$i], null
                    );
                } else if (is_array($x[$i])) {
                    $arr = $x[$i];

                    if (isset($arr["alias"])) {
                        //echo $arr["alias"] . "<br>";
                        $l = &$this->fieldExists($arr["alias"]);
                        if ($l)
                            throw new \Exception("Duplicate field with a given alias '" . $arr["alias"] . "'");
                    }

                    $l = &$this->fieldExists($arr["name"]);
                    if ($l) {
                        if (isset($arr["alias"])) {
                            //echo json_encode($l) . " - " . $arr["alias"] . "<br>";
                            $l[1] = $arr["alias"];
                        }
                        continue;
                    };

                    if (isset($arr["table"])) {
                        $alias = isset($arr["alias"]) ? $arr["alias"] : null;
                        $this->addFieldIntoTable($arr["table"], $arr["name"], $alias);
                    } else {
                        $this->addFieldIntoTable($this->tablesWithFields[0]["name"], $arr["name"], $arr["alias"]);
                    }
                }
            }
        };

        // Check if group is present.
        if ( $this->group ) {

            // Check if by parameter is/is not present.
            if ( !isset( $this->group["by"] ) )
                throw new \Exception("Cannot group - missing 'by' parameter");

            // Process aggregate functions if present.
            if (isset($this->group["fields"])) {
                if (is_array($this->group["fields"])) {
                    for ($i = 0; $i < count($this->group["fields"]); $i++) {
                        if ($this->group["fields"][$i] instanceof AggregateFunction) {
                            $this->group["fields"][$i]->setQuery($this);

                            $this->group["fields"][$i]->checkFieldExistance($this->group["by"]);

                            $this->tablesWithFields[0]["fields"][] = array(
                                null, $this->group["fields"][$i]->getAlias()
                            );
                            array_push($this->aggregateFields, $this->group["fields"][$i]);
                        };
                    }
                } else if ($this->group["fields"] instanceof AggregateFunction) {
                    $this->group["fields"]->setQuery($this);
                    $this->group["fields"]->checkFieldExistance($this->group["by"]);
                    $this->tablesWithFields[0]["fields"][] = array(
                        null, $this->group["fields"]->getAlias()
                    );
                    array_push($this->aggregateFields, $this->group["fields"]);
                };
            }
        };

        $this->isCreatedMetadata = true;

        //echo json_encode( $this->tablesWithFields ) . "<br>";
    }

    public function fieldExistsWithinGroupElements( string $field ): bool {
        $found = false;
        for ($i = 0; $i < count( $this->aggregateFields ) && !$found; $i++) {
            if ( $this->aggregateFields[$i]->getAlias() === $field )
                $found = true;
        }
        return $found;
    }

    protected function addField( string $table, string $field, ?string $alias = null ): void {
        $this->tableFields[] = array(
            "field" => $field,
            "alias" => $alias,
            "table" => $table
        );
    }

    protected function addFieldIntoTable( string $table, string $field, ?string $alias = null ): void {
        $added = false;
        for ( $i = 0; $i < count( $this->tablesWithFields ); $i++ ) {
            if ( $this->tablesWithFields[$i]["name"] === $table ||
                $this->tablesWithFields[$i]["alias"] === $table
            ) {
                $this->tablesWithFields[$i]["fields"][] = array(
                    $field, $alias
                );
                $added = true;
            }
        }
    }

    protected function getTableForField( string $field ): string {
        $s = "`";
        $t = false;
        for ( $i = 0; $i < count( $this->tablesWithFields ); $i++ ) {
            for ( $j = 0; $j < count( $this->tablesWithFields[$i]["fields"] ); $j++ ) {
                if ( $this->tablesWithFields[$i]["fields"][$j] === $field ) {
                    $s .= $this->tablesWithFields[$i]["alias"] ? $this->tablesWithFields[$i]["alias"] : $this->tablesWithFields["name"];
                    $t = true;
                };
            }
        }
        if ( !$t )
            $s .= "UNDEFINED";
        $s .= "`";
        return $s;
    }

    protected function _addNewTable( string $name, ?string $alias, bool $is_nested ): void {
        $found = false;
        for ( $i = 0; $i < count( $this->tablesWithFields ) && $found; $i++ ) {
            if ( $this->tablesWithFields[$i]["table"] == $name ||
                $alias && $this->tablesWithFields[$i]["alias"] == $alias
            ) {
                $found = true;
            }
        };
        if ( !$found ) {
            $this->_addTable($name, $alias, array(), $is_nested);
        }
    }

    protected function _addTable( string $name, ?string $alias, array $fields, bool $is_nested ): void {
        array_push( $this->tablesWithFields, array(
            "name" => &$name,
            "alias" => &$alias,
            "fields" => &$fields,
            "is_nested" => $is_nested
        ) );
    }

    public function getFullField( string $field, int $position = self::PART_FIELDS, ?string $table = null ): string {
        $s = "`";
        $isNested = false;
        $alias = null;
        $found = false;
        //echo json_encode( $this->tablesWithFields ) . "<br>";

        if ( $table ) {
            if ( !$this->fieldExistsInTheTable( $table, $field ) )
                throw new \Exception("Field '$field' does not exist within the table '$table'");
            else {
                return "`$table`.`$field`";
            }
        } else if ( !str_starts_with($field, "field::") ) {
            // Get table name.
            for ($i = 0; $i < count($this->tablesWithFields) && !$found; $i++) {
                for ($j = 0; $j < count($this->tablesWithFields[$i]["fields"]) && !$found; $j++) {
                    if (
                        $this->tablesWithFields[$i]["fields"][$j][1] == $field ||
                        $this->tablesWithFields[$i]["fields"][$j][0] == $field
                    ) {
                        $alias = $this->tablesWithFields[$i]["fields"][$j][1];
                        if ($this->tablesWithFields[$i]["is_nested"]) {
                            $s .= $this->tablesWithFields[$i]["name"];
                            $isNested = true;
                            //echo "$field-$alias<br>";
                        } else {
                            //echo "$field<br>";
                            $s .= $this->tablesWithFields[$i]["name"];
                        }
                        $found = true;
                    };
                }
            }
            // Check if field is found within all tables.
            if ( !$found )
                throw new \Exception("Field '$field' does not exist within the query");

            $s .= "`.";
            if ( $alias !== null && $position >= self::PART_WHERE ) {
                $s = "`" . $alias . "`";
            } else {
                $s .= "`" . $field . "`";
            }
            return $s;
        } else {
            $arr = get_table_with_field($field);
            if ( !$arr )
                throw new \Exception("Field '$field' does not exist within the query");
            return get_full_field($arr);
        }
    }

    public function &fieldExists( string $field ): ?array {
        $var = null;
        if ( str_starts_with($field, "field::") ) {
            $arr = get_table_with_field($field);
            $x = &$this->fieldExistsInTheTable($arr[0], $arr[1]);
            echo json_encode($this->tablesWithFields) . "<br>";
            echo json_encode($arr) . "<br>";
            echo json_encode($x);
            return $x;
        } else {
            for ($i = 0; $i < count($this->tablesWithFields); $i++) {
                for ($j = 0; $j < count($this->tablesWithFields[$i]["fields"]); $j++) {
                    if ($this->tablesWithFields[$i]["fields"][$j][0] == $field ||
                        $this->tablesWithFields[$i]["fields"][$j][1] == $field
                    ) {
                        return $this->tablesWithFields[$i]["fields"][$j];
                    };
                }
            }
        };
        return $var;
    }

    public function &fieldExistsInTheTable( string $table, string $field ): ?array {
        $var = null;
        for ($i = 0; $i < count( $this->tablesWithFields ); $i++ ) {

            if ( $this->tablesWithFields[$i]["name"] != $table &&
                $this->tablesWithFields[$i]["alias"] != $table
            )
                continue;

            for ($j = 0; $j < count( $this->tablesWithFields[$i]["fields"] ); $j++ ) {
                if ( $this->tablesWithFields[$i]["fields"][$j][0] == $field ||
                    $this->tablesWithFields[$i]["fields"][$j][1] == $field
                ) {
                    return $this->tablesWithFields[$i]["fields"][$j];
                };
            }
        }
        return $var;
    }

    public function getAllTableFields(): array {
        $a = array();
        for ($i = 0; $i < count( $this->tablesWithFields ); $i++ ) {
            for ($j = 0; $j < count( $this->tablesWithFields[$i]["fields"] ); $j++ ) {
                array_push($a, $this->tablesWithFields[$i]["fields"][$j]);

                // Reset alias of the field.
                if ( $this->isNested && $a[$j][1] ) {
                    $a[$j][0] = $a[$j][1];
                    $a[$j][1] = null;
                };
            }
        }
        return $a;
    }

    protected function checkQueryParameters(): void {
        if ( $this->condition )
            $this->condition->performCheck();
    }

    public function &_execute(array $params): array|null
    {
        $str = $this->getQueryString();
        $stmt = $this->connection->prepare($str, $params);
        $stmt->execute();
        $arr = $stmt->fetchAll( PDO::FETCH_CLASS );
        return $arr;
    }

    public function forEach( closure $closure ): void
    {
        $arr = &$this->execute();
        for ($i = 0; $i < count( $arr ); $i++)
            $closure->call($this, $arr[$i]);
    }

    public function isNested(): bool {
        return $this->isNested;
    }

    public function setNested( bool $nested ): void {
        $this->isNested = $nested;
    }

}