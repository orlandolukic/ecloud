<?php

namespace Core\Database;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class MinAggregateFunction extends AggregateFunction {

    public function __construct(string $field, string $alias)
    {
        parent::__construct("MIN", $field, $alias);
    }

    protected function &_printContent(): string
    {
        if ( str_starts_with($this->field, "field::") ) {
            $arr = get_table_with_field($this->field);
            $field = "`$arr[0]`.`$arr[1]`";
        } else {
            $field = $this->query->getFullField($this->field);
        }
        $str = "MIN(" . $field . ")";
        return $str;
    }
}
