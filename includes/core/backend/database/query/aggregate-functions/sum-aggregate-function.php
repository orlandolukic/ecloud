<?php

namespace Core\Database;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class SumAggregateFunction extends AggregateFunction {

    public function __construct(?string $field, string $alias)
    {
        parent::__construct("SUM", $field, $alias);
    }

    protected function &_printContent(): string
    {
        if ( str_starts_with($this->field, "field::") ) {
            $arr = get_table_with_field($this->field);
            $field = "`$arr[0]`.`$arr[1]`";
        } else {
            $field = $this->query->getFullField($this->field);
        }
        $t =  "SUM(" . $field . ")";
        return $t;
    }
}
