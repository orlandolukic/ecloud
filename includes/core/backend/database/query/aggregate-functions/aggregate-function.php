<?php

namespace Core\Database;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

abstract class AggregateFunction {

    protected ?string $type;
    protected string $alias;
    protected string $field;
    protected ?SelectQuery $query;

    public function __construct( string $type, string $field, string $alias )
    {
        $this->alias = $alias;
        $this->field = $field;
        $this->query = null;
        $this->type = $type;
    }

    public function checkFieldExistance( array $fields ): void {
        if ( $this->type === "COUNT" && str_starts_with($this->field, "*") )
            return;

        if ( !in_array($this->field, $fields) )
            throw new \Exception("Field '$this->field' is not listed within the group fields");
    }

    /**
     * Sets the query for this aggregate function.
     *
     * @param SelectQuery $query
     */
    public function setQuery( SelectQuery $query ): void {
        $this->query = $query;
    }

    /**
     * Prints content of the aggregate function.
     * @return string
     */
    protected abstract function &_printContent(): string;

    public function &printContent(): string {
        $s = &$this->_printContent();
        if ( $this->alias )
            $s .= " AS " . $this->alias;
        return $s;
    }

    /**
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @return string|null
     */
    public function getField(): ?string
    {
        return $this->field;
    }

    /**
     * Gets aggregate function's name.
     *
     * @return string Name of the aggregate function.
     */
    public function getAggregateFunction(): string {
        return $this->type;
    }
}