<?php

namespace Core\Database;

// Silence is golden!
use Core\DataStructures\LinkedList;

if ( !defined("APP_REQ") )
    exit();

class OrderByClause {

    public const ASC = 1;
    public const DESC = -1;

    private array $params;
    private SelectQuery $query;

    public static function single( string $name, int $orderType ): OrderByClause {
        return new OrderByClause(array(
            array( $name, $orderType )
        ));
    }

    public function __construct( array $arr )
    {
        $this->params = $arr;
    }

    public function printOrderClause(): string {
        $text = " ORDER BY ";
        $n = count($this->params);
        for ($i = 0; $i<$n; $i++) {
            $text .= $this->query->getFullField( $this->params[$i][0], SelectQuery::PART_ORDERBY );
            $text .= " ";
            $text .= $this->params[$i][1] === self::ASC ? "ASC" : "DESC";
            if ( $i + 1 < $n )
                $text .= ", ";
        }
        return $text;
    }

//    private static function _part(string &$text, array $parts ): void {
//
//        for ($i = 0; $i<count($parts); $i++) {
//            $text .=
//        }
//    }

    public function assignSelectQuery( SelectQuery $query ): void {
        $this->query = $query;
    }
}
