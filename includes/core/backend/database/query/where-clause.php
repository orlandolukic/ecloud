<?php

namespace Core\Database;

// Silence is golden!
use Core\DataStructures\LinkedList;

if ( !defined("APP_REQ") )
    exit();

class WhereClause {

    public static function single( string $field, string $operator, mixed $value = null ): WhereClause {
        return new WhereClause(array(
            "type" => "and",
            "parts" => array(
                new WherePart($field, $operator, $value)
            )
        ));
    }
    private static function _part(int $depth, string &$text, array $parts, ?string $connector ): void {

        if ( isset( $parts["type"] ) && isset( $parts["parts"] ) ) {
            self::_part($depth+1, $text, $parts["parts"], $parts["type"]);
            return;
        }

        if ( $depth > 0 && !$connector )
            throw new \Exception("Connector must be provided in nested where clause");

        for ($i = 0; $i<count($parts); $i++) {
            if ( $i > 0 ) {
                $text .= " " . strtoupper($connector) . " ";
            };
            if ( $parts[$i] instanceof WherePart ) {
                if ( $depth > 1 )
                    $text .= "(";
                $text .= $parts[$i]->getWherePart();
                if ( $depth > 1 )
                    $text .= ")";
            } else {
                $text .= "(";
                self::_part(0, $text, $parts[$i], $connector);
                $text .= ")";
            }
        }
    }

    // ===========================================================================================================

    private array $params;
    private SelectQuery $query;
    private int $part;
    private LinkedList $list;

    public function __construct( array $arr )
    {
        $this->params = $arr;
        $this->part = SelectQuery::PART_WHERE;
        $this->list = new LinkedList();
    }

    public function performCheck(): void {

    }

    public function printWherePart(): string {
        $text = "";
        self::_part( 0, $text, $this->params, null );
        return $text;
    }

    public function setPart( int $part ): void {
        $list = new LinkedList();
        $list->addLast($this->params);
        while( $list->size() > 0 ) {
            $a = $list->removeFirst();
            if ( $a instanceof WherePart ) {

                $a->setPart($part);

            } elseif ( isset( $a["parts"] ) ) {
                $parts = $a["parts"];
                for ($i = 0; $i < count($parts); $i++)
                    $list->addLast( $parts[$i] );
            };
        }
    }

    public function assignSelectQuery( SelectQuery $query ): void {
        $this->query = $query;
        $list = new LinkedList();
        $list->addLast($this->params);
        while( $list->size() > 0 ) {
            $a = $list->removeFirst();
            if ( $a instanceof WherePart ) {
                $a->assignSelectQuery($query);
            } elseif ( isset( $a["parts"] ) ) {
                $parts = $a["parts"];
                for ($i = 0; $i < count($parts); $i++)
                    $list->addLast( $parts[$i] );
            };
        }
    }
}