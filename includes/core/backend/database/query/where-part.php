<?php

namespace Core\Database;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class WherePart {

    public const OPERATOR_EQUAL = "=";
    public const OPERATOR_NOT_EQUAL = "!=";
    public const OPERATOR_GT = ">";
    public const OPERATOR_GTE = ">=";
    public const OPERATOR_LT = "<";
    public const OPERATOR_LTE = "<=";
    public const OPERATOR_NOT_NULL = "IS NOT NULL";
    public const OPERATOR_NULL = "IS NULL";
    public const OPERATOR_LIKE = "LIKE";

    private string $field;
    private string $operator;
    private mixed $value;
    private SelectQuery $query;
    private int $part;

    public function __construct( string $field, string $operator, mixed $value = null )
    {
        $this->field = $field;
        $this->operator = $operator;
        $this->value = $value;
        $this->bothFields = false;
        $this->part = SelectQuery::PART_WHERE;
    }

    public function assignSelectQuery( SelectQuery $query ) {
        $this->query = $query;
    }

    public function getWherePart(): string {
        if ( str_starts_with($this->field, "field::") ) {
            $val = str_replace("field::", "", $this->field);
            $val = preg_split("/\./", $val);
            $t = "";
            for ($i = 0; $i<count($val); $i++) {
                $t .= "`";
                $t .= $val[$i];
                $t .= "`";
                if ( $i+1 < count( $val ) )
                    $t .= ".";
            }
        } else
            $t =  $this->query->getFullField( $this->field, SelectQuery::PART_WHERE );
        $t .= " " . $this->operator . " ";
        $this->printValue($t);
        return $t;
    }

    private function printValue( string &$t ): void {
        if ( $this->value != null ) {
            if ( is_string($this->value) ) {
                if ( str_starts_with($this->value, "field::") ) {
                    $v = str_replace("field::", "", $this->value);
                    $g = preg_split( "/\./", $v );
                    if ( $g && count($g) ) {
                        for ($i = 0; $i < count($g); $i++) {
                            $t .= "`" . $g[$i] . "`";
                            if ( $i+1<count($g) )
                                $t .= ".";
                        }
                    } else
                        $t .= $v;
                } else if ( str_starts_with($this->value, "::") ) {
                    $v = str_replace("::", ":", $this->value);
                    $t .= $v;
                } else
                    $t .= "'" . $this->value . "'";
            } else if ( is_numeric( $this->value ) )
                $t .= $this->value;
        }
    }

    /**
     * Sets query position.
     *
     * @param int $part
     */
    public function setPart( int $part ): void {
        $this->part = $part;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        return $this->value;
    }
}