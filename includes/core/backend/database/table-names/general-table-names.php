<?php

namespace Core\Database;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class GeneralTableNames {

   public const USERS = "users";

}