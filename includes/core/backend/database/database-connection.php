<?php

namespace Core\Database;

use PDO;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class DatabaseConnection {

    private string $username;
    private string $password;
    private string $host;
    private string $dbname;
    private bool $connected;
    private ?PDO $connection;

    public function __construct( string $host, string $dbname, string $username, string $password )
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->dbname = $dbname;
        $this->connected = false;
        $this->connection = null;
    }

    /**
     * Connects to database.
     */
    public function connect(): void {

        if ( $this->connected )
            return;

        $this->connection = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->dbname, $this->username, $this->password);
        $this->connected = true;
    }

    public function disconnect(): void {
        if ( !$this->connected )
            return;
        $this->connection = null;
        $this->connected = false;
    }

    /**
     * @return string
     */
    public function getDatabaseName(): string
    {
        return $this->dbname;
    }

    /**
     * @return bool
     */
    public function isConnected(): bool
    {
        return $this->connected;
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }

    public function equals( string $dbname, string $username, string $password ): bool {
        return $username === $this->username && $password === $this->password &&
            $dbname === $this->dbname;
    }


}