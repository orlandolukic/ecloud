<?php

function funkcija1( string $x ): string {
    $str = str_replace("AsyncManager", "B", $x);
    return $str;
}
add_filter('filter-a', 'funkcija1' );

function funkcija2( string $x ): string {
    $str = str_replace("B", "C", $x);
    return $str;
}
add_filter('filter-a', 'funkcija2' );

echo apply_filter('filter-a', "AXAXAX" );

$t = array(
    "a" => 10,
    "b" => 20
);

function funkcija3( array $arr ): array {
    $arr["c"] = 250;
    return $arr;
}
add_filter( 'filter-b', 'funkcija3' );

function funkcija4( array $arr ): array {
    $arr["d"] = 25000;
    $arr["a"] = -1;
    return $arr;
}
add_filter( 'filter-b', 'funkcija4' );

echo "<br>";

echo json_encode( apply_filter('filter-b', $t ) );