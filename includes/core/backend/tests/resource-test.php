<?php

if ( !defined("APP_REQ") )
    exit();

enqueue_script( 'r', INCLUDES_URI . "/r.js", ['f','rtx','b'], time() );
enqueue_script( 'b', INCLUDES_URI . "/b.js", ['jquery', 'a'], time() );
enqueue_script( 'jquery', INCLUDES_URI . "/jquery.js" );
enqueue_script( 'a', INCLUDES_URI . "/a.js", ['jquery'], time() );
enqueue_script( 'c', INCLUDES_URI . "/c.js", ['rtx'], time() );
enqueue_script( 'rtx', INCLUDES_URI . "/rtx.js", ['b'], time() );
enqueue_script( 'f', INCLUDES_URI . "/f.js", ['a','b','c'], time() );

enqueue_style('global', INCLUDES_URI . "/css/global.css", null, null);
enqueue_style('x1', INCLUDES_URI . "/css/x1.css", ['global'], null);