<?php

if ( !defined("APP_REQ") )
    exit();

require 'single-filter.php';
require 'filter-center.php';

function add_filter( string $name, array|string|callable|closure $function, int $priority = 10, int $acceptedArgs = 1 ): void {
    \Core\FilterCentar::getInstance()->registerFilter( $name, $priority, $function, $acceptedArgs );
}

function apply_filter( string $name, ...$args ): mixed {
    return \Core\FilterCentar::getInstance()->applyFilter( $name, $args );
}

