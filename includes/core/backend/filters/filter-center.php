<?php

namespace Core;

use Closure;
use JetBrains\PhpStorm\Pure;

if ( !defined("APP_REQ") )
    exit();

class FilterCenterElement {
    private SingleFilter $filter;
    private ?self $next;

    public function __construct( SingleFilter $action )
    {
        $this->filter = $action;
        $this->next = null;
    }

    public function getFilter(): SingleFilter
    {
        return $this->filter;
    }

    public function getNext(): ?FilterCenterElement
    {
        return $this->next;
    }

    public function setNext(?FilterCenterElement $next): void
    {
        $this->next = $next;
    }
}

class FilterCentar {

    private static ?self $instance = null;
    public static function getInstance(): self {
        if ( self::$instance == null )
            self::$instance = new self();
        return self::$instance;
    }

    private ?FilterCenterElement $filtersFirst;
    private ?FilterCenterElement $filtersLast;
    private int $filtersCount;

    private function __construct() {
        $this->filtersFirst = null;
        $this->filtersLast = null;
        $this->filtersCount = 0;
    }

    public function registerFilter(string $filterName, int $priority, array|string|callable|closure $function, int $acceptedArgs ): void {
        $filter = $this->_hasFilterInTheCenter($filterName);
        if ( $filter != null ) {
            $filter->registerFilter($priority, $function, $acceptedArgs);
        } else {
            $newFilter = new SingleFilter($filterName);
            $newFilterActionCenterElement = new FilterCenterElement( $newFilter );
            $newFilter->registerFilter($priority, $function, $acceptedArgs);
            if ( !$this->filtersFirst ) {
                $this->filtersFirst = $this->filtersLast = $newFilterActionCenterElement;
            } else {
                $this->filtersLast->setNext($newFilterActionCenterElement);
                $this->filtersLast = $newFilterActionCenterElement;
            }
            $this->filtersCount++;
        }
    }

    public function deregisterFilter( string $filterName, array|string $function ): void {
        $filter = $this->_hasFilterInTheCenter($filterName);
        if ( $filter != null ) {
            $filter->deregisterCallaback( $function );
        };
    }

    public function applyFilter( string $filterName, ...$args ): mixed {
        $argsArr = func_get_args();
        $current = $this->filtersFirst;
        while( $current != null ) {
            if ( strcmp( $current->getFilter()->getFilterName(), $filterName ) === 0 ) {
                $arr = array(); $m = 0;
                for ($i = 1; $i<count($argsArr); $i++)
                    $arr[$m++] = $argsArr[$i];
                $arr = $arr[0];
                return $current->getFilter()->applyFilter($arr);
            };
            $current = $current->getNext();
        }
        return null;
    }

    #[Pure] private function _hasFilterInTheCenter(string $filterName ): ?SingleFilter  {
        $action = null;
        $current = $this->filtersFirst;
        while( $current != null && $action == null ) {
            if ( strcmp( $current->getFilter()->getFilterName(), $filterName ) === 0 )
                $action = $current->getFilter();
            $current = $current->getNext();
        }
        return $action;
    }

}