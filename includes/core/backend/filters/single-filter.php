<?php
namespace Core;

use Closure;

if ( !defined("APP_REQ") )
    exit();

class SingleFilterElement {
    private int $priority;
    private array|string|closure $function;
    private int $acceptedArgs;
    private self|null $next;

    public function __construct( int $priority, array|string|closure $function, int $acceptedArgs )
    {
        $this->priority = $priority;
        $this->function = $function;
        $this->acceptedArgs = $acceptedArgs;
        $this->next = null;
    }

    public function getPriority(): int {
        return $this->priority;
    }

    public function getFunction(): array|string|closure {
        return $this->function;
    }

    public function getNext(): self|null {
        return $this->next;
    }

    public function setNext( $next ): void {
        $this->next = $next;
    }
}

class SingleFilter {
    private string $name;
    private SingleFilterElement|null $firstFilter;
    private int $callableNumber;

    public function __construct( string $name )
    {
        $this->firstFilter = null;
        $this->name = $name;
        $this->callableNumber = 0;
    }

    public function applyFilter( array $parameters ): mixed {
        $current = $this->firstFilter;
        $value = null;
        while( $current != null ) {
            $value = call_user_func_array(  $current->getFunction(), $parameters );
            $parameters[0] = $value;
            $current = $current->getNext();
        }
        return $parameters[0];
    }

    public function registerFilter(int $priority, array|string|callable|closure $function, int $acceptedArgs ): void {
        $elem = new SingleFilterElement($priority, $function, $acceptedArgs);
        if ( !$this->firstFilter ) {
            $this->firstFilter = $elem;
        } else {
            $previous = null;
            $current = $this->firstFilter;
            while( $current != null && $current->getPriority() <= $priority ) {
                $previous = $current;
                $current = $current->getNext();
            }
            if ( $current == null ) {
                $previous->setNext( $elem );
            } else {
                $elem->setNext( $current );
                if ( $previous == null ) {
                    $elem->setNext( $this->firstFilter );
                    $this->firstFilter = $elem;
                } else
                    $previous->setNext( $elem );
            }
        };
        $this->callableNumber++;
    }

    public function deregisterCallaback( array|string|callable|closure $function ): void {
        $previous = null;
        $current = $this->firstFilter;
        $deleted = false;
        while( $current != null && !$deleted ) {
            if ( $current->getFunction() == $function ) {
                if ( $previous == null ) {
                    $this->firstFilter = $this->firstFilter->getNext();
                } else
                    $previous->setNext( $current->getNext() );
                $deleted = true;
                $this->callableNumber--;
            };
            $previous = $current;
            $current = $current->getNext();
        }
    }

    public function getCallableNumber(): int {
        return $this->callableNumber;
    }

    public function getFilterName(): string {
        return $this->name;
    }
}