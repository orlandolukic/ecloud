<?php

require "locales-manager.php";

function load_new_locale( string $locale ): void {
    \Core\LocalesManager::getLocalesManager()->changeLocale($locale);
}

function __( string $search_term ): string {
    return \Core\LocalesManager::getLocalesManager()->getTranslationEntryFor($search_term);
}

function ajax_append_translation( string $search_term, bool $deep = false ): void {
    \Core\LocalesManager::getLocalesManager()->appendAjax($search_term, $deep);
}