<?php
/**
 * Manager for locales
 */
namespace Core;

use JetBrains\PhpStorm\Pure;

if ( !defined('APP_REQ') )
    exit();

class LocalesManager {

    private static string $START_LOCALE = "sr-cyrilic";

    private static ?self $instance = null;
    private string $search_term;

    public static function getLocalesManager(): self {
        if ( self::$instance == null ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private ?string $currentLocale;
    private ?object $readValues;

    private function __construct() {
        $this->currentLocale = self::$START_LOCALE;
        $this->readValues = null;
        $this->_readJSONContent();
    }

    public function changeLocale( string $locale ): void {
        if ( $this->_checkIfLocaleExists($locale) || strcmp( $locale, $this->currentLocale ) === 0 )
            return;
        $this->currentLocale = $locale;
    }

    public function getTranslationEntryFor( string $key ): string {
        if ( $this->readValues == null )
            return "";

        $arr = preg_split("/\\./", $key);
        $object = get_object_vars( $this->readValues );
        if ( !array_key_exists( $arr[0], $object ) )
            return "Undefined global key $key";

        $object = $object[ $arr[0] ];
        $str = "$arr[0]";
        for ( $i = 1; $i < count($arr); $i++ ) {
            $object = get_object_vars( $object );
            $str .= ".$arr[$i]";
            if ( !array_key_exists( $arr[$i], $object ) )
                return "Undefined translation unit for '$str'";
            $object = $object[ $arr[$i] ];
        }

        return $object;
    }

    #[Pure] public function _keyExists( string $key ): bool {
        $arr = preg_split("/\\./", $key);
        $object = get_object_vars( $this->readValues );
        if ( !array_key_exists( $arr[0], $object ) )
            return false;

        $object = $object[ $arr[0] ];
        for ( $i = 1; $i < count($arr); $i++ ) {
            $object = get_object_vars($object);
            if (!array_key_exists($arr[$i], $object))
                return false;
            $object = $object[ $arr[$i] ];
        };
        return true;
    }

    #[Pure] private function _checkIfLocaleExists(string $locale ): bool {
        return file_exists( ROOT_URL . "/locales/" . $locale . ".json" );
    }

    private function _readJSONContent(): void {
        $content = file_get_contents ( ROOT_URL . "/locales/" . $this->currentLocale . ".json" );
        $this->readValues = json_decode( $content );
    }

    /**
     * Adds ajax term in object
     *
     * @param string $search_term
     * @param bool $deep
     */
    public function appendAjax(string $search_term, bool $deep): void
    {
        $this->search_term = $search_term;
        add_filter( 'ajax_general', function($obj): array {
            if ( !isset( $obj["translations"] ) )
                $obj["translations"] = array();
            $obj["translations"][$this->search_term] = $this->getTranslationEntryFor($this->search_term);
            return $obj;
        });
    }
}