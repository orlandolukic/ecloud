<?php

if ( !defined('APP_REQ') )
    exit();

require 'pages/page.php';
require 'pages/authorized-page.php';
require 'pages/page-manager.php';
require 'resources/resources.php';

function ecloud_head(): void {
    ?>
<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" type="image/png" href="/assets/images/logo.png">

    <?php do_action('enqueue_styles'); ?>

    <?php do_action('ecloud_head'); ?>
<?php
}

// Enqueue all important styles & scripts for frontend to work.
enqueue_style( 'business-min', ASSETS_URI . "/css/business-min.css", ['bootstrap-grid'] );
enqueue_style( 'font-awesome', ASSETS_URI . "/css/font-awesome.css" );
enqueue_style( 'bootstrap-grid', ASSETS_URI . "/css/bootstrap-grid.min.css" );
enqueue_script( 'core-min-js', ASSETS_URI . "/js/core-min.js", ['jquery'] );
enqueue_script( 'jquery', ASSETS_URI . "/js/jquery-3.6.0.min.js", null, "3.6.0" );

// Create system variable.
add_action( 'system_variable', 10, array( '\Core\PageManager', 'writeSystemVariable' ) );