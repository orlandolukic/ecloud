<?php

if ( !defined( 'APP_REQ' ) )
    exit();

require 'resource-manager.php';

function enqueue_script( string $id, string $path, ?array $deps = null, ?string $version = null ): void {
    $resource = new \Core\ScriptResource( $id, $path, $deps, $version );
    \Core\ResourceManager::getScriptsManager()->registerResource($resource);
}

function enqueue_style( string $id, string $path, ?array $deps = null, ?string $version = null ): void {
    $resource = new \Core\StyleResource( $id, $path, $deps, $version );
    \Core\ResourceManager::getStylesManager()->registerResource($resource);
}

//include INCLUDES_URL . "/core/tests/resource-test.php";