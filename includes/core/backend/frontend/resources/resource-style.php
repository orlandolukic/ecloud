<?php

namespace Core;

class StyleResource extends Resource {

    private ?string $version;

    public function __construct( string $id, string $path, ?array $dependencies, ?string $version)
    {
        parent::__construct($id, $path, $dependencies);
        $this->version = $version;
    }

    public function html(): string
    {
        ob_start();
        $version = "?v=";
        if ( $this->version != null ) {
            $version .= $this->version;
        } else {
            if ( Resource::CLEAR_CACHE )
                $version .= time();
            else
                $version .= Resource::VERSION;
        }
        ?>
        <link id="stylesheet-<?= $this->id ?>" rel="stylesheet" type="text/css" href="<?= $this->path . $version ?>">
        <?php
        $s = preg_replace("/(\r\n|\\t{2,})/", "", ob_get_clean());
        $s = preg_replace("/(\\s|\\t){2,}/", " ", $s);
        return $s;
    }
}