<?php

namespace Core;

use JetBrains\PhpStorm\Pure;

if ( !defined('APP_REQ') )
    exit();

class ResourceListElement {
    private \Core\Resource $resource;
    private ?ResourceListElement $next;

    /**
     * ResourceListElement constructor.
     * @param Resource $resource
     */
    public function __construct( \Core\Resource $resource )
    {
        $this->resource = $resource;
    }

    /**
     * @return Resource
     */
    public function getResource(): \Core\Resource
    {
        return $this->resource;
    }

    /**
     * @return ResourceListElement|null
     */
    public function getNext(): ?ResourceListElement
    {
        return $this->next;
    }

    /**
     * @param ResourceListElement|null $next
     */
    public function setNext(?ResourceListElement $next): void
    {
        $this->next = $next;
    }
}

abstract class Resource {

    public const CLEAR_CACHE = true;
    public const VERSION = "1.0.0";

    protected string $id;
    protected string $path;
    protected ?array $dependencies;

    /**
     * Resource constructor.
     * @param string $id
     * @param string $path Path to the resource (URI)
     * @param array|null $dependencies
     */
    public function __construct( string $id, string $path, ?array $dependencies )
    {
        $this->id = $id;
        $this->path = $path;
        $this->dependencies = $dependencies;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getDependencies(): ?array
    {
        return $this->dependencies;
    }

    /**
     * @return int
     */
    #[Pure] public function getDependencyCount(): int {
        return count($this->dependencies);
    }

    /**
     * @param string $id
     */
    public function removeDependency( string $id ): void {
        $deleted = 0;
        $arr = array();
        $m = 0;
        for ($i = 0; $i < count($this->dependencies); $i++) {
            if ( strcmp( $this->dependencies[$i], $id ) == 0 ) {
                $deleted++;
            } else {
                $arr[$m++] = $this->dependencies[$i];
            }
        }
        if ( $deleted > 0 ) {
            $this->dependencies = $arr;
        };
    }

    /**
     * @param string $id
     * @return bool
     */
    public function hasDependency( string $id ): bool {

        if ( strcmp( $id, $this->id ) == 0 )
            return false;

        for ($i = 0; $i < count($this->dependencies); $i++) {
            if ( strcmp( $this->dependencies[$i], $id ) == 0 )
                return true;
        }
        return false;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    /**.
     * @return string Print of the resource (HTML like)
     */
    public abstract function html(): string;
}