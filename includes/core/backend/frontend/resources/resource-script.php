<?php

namespace Core;

use JetBrains\PhpStorm\Pure;

class ScriptResource extends Resource {

    private ?string $version;

    public function __construct( string $id, string $path, ?array $dependencies, ?string $version)
    {
        parent::__construct($id, $path, $dependencies);
        $this->version = $version;
    }

    public function html(): string
    {
        ob_start();
        $version = "?v=";
        if ( $this->version != null ) {
            $version .= $this->version;
        } else {
            if ( Resource::CLEAR_CACHE )
                $version .= time();
            else
                $version .= Resource::VERSION;
        }
        ?>
        <script id="script-<?= $this->id ?>" src="<?= $this->path . $version ?>" type="text/javascript" charset="utf-8"></script>
        <?php
        return ob_get_clean();
    }
}