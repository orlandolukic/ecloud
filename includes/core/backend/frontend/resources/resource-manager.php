<?php

namespace Core;

if ( !defined('APP_REQ') )
    exit();

require 'resource.php';
require 'resource-script.php';
require 'resource-style.php';

class ResourceManager {

    private const SCRIPTS_MANAGER = 1;
    private const STYLES_MANAGER = 2;

    private static ?self $scriptsManagerInstance = null;
    private static ?self $stylesManagerInstance = null;
    public static function getScriptsManager(): self {
        if ( self::$scriptsManagerInstance == null )
            self::$scriptsManagerInstance = new self(self::SCRIPTS_MANAGER);
        return self::$scriptsManagerInstance;
    }

    public static function getStylesManager(): self {
        if ( self::$stylesManagerInstance == null )
            self::$stylesManagerInstance = new self(self::STYLES_MANAGER);
        return self::$stylesManagerInstance;
    }

    private ?ResourceListElement $importListFirst;
    private ?ResourceListElement $importListLast;
    private ?ResourceListElement $temporaryList;
    private bool $actionExecuted;

    private function __construct( int $type )
    {
        $this->importListFirst = null;
        $this->temporaryList = null;
        $this->importListLast = null;
        $this->actionExecuted = false;

        if ( $type == self::SCRIPTS_MANAGER )
            add_action( 'enqueue_scripts', 0, array( $this, 'enqueue_resources' ), null );
        else if ( $type == self::STYLES_MANAGER )
            add_action( 'enqueue_styles', 0, array( $this, 'enqueue_resources' ), null );
    }

    public function enqueue_resources(): void {

        if ( $this->actionExecuted )
            return;

        $current = $this->importListFirst;
        while( $current != null ) {
            echo $current->getResource()->html();
            $current = $current->getNext();
        };
        $this->actionExecuted = true;

        $current = $this->temporaryList;
        $deps = null;
        while( $current != null ) {
            $deps = $current->getResource()->getDependencies();
            //if ( count( $deps ) > 0 )
                echo "<!-- Resource '" . $current->getResource()->getId() . "' import failed: Missing dependencies " . json_encode($deps) . " -->";
            $current = $current->getNext();
        }

    }

    /**
     * @param Resource $resource
     */
    public function registerResource( \Core\Resource $resource ): void {
        $elem = new ResourceListElement( $resource );
        $dependencies = $elem->getResource()->getDependencies();
        if ( $dependencies == null || count( $dependencies ) == 0 ) {

            // Add new element to the beginning of the list.
            $this->_addResourceInTheList( "import-list", null, $elem, true );

            // Update all dependencies in $temporaryList.
            $this->_updateDependenciesInTemporaryList($elem);

        } else {

            // Check if all dependencies are already loaded.
            if ( !$this->_checkIfAllDependenciesAreLoaded($elem) )
                // Add element in the temporary list (at the beginning).
                $this->_addResourceInTheList( "temporary-list", null, $elem, true );
            else {
                // Add element in the regular list (at the end).
                $this->_addResourceInTheList("import-list", $this->importListLast, $elem, true);

                // Update all dependencies in $temporaryList.
                $this->_updateDependenciesInTemporaryList($elem);
            };
        }
//        echo $id . "<br>";
//        echo $this->_dump_list('temporary-list') . "<br>";
    }

    private function _checkIfAllDependenciesAreLoaded( ResourceListElement $element ): bool {
        $deps = $element->getResource()->getDependencies();
        for ($i = 0; $i < count( $deps ); $i++) {
            $current = $this->importListFirst;
            $dep = $deps[$i];
            $found = false;
            while( $current != null ) {
                if ( $dep == $current->getResource()->getId() )
                    $found = true;
                $current = $current->getNext();
            };
            if ( !$found )
                return false;
        };
        return true;
    }

    private function _addResourceInTheList( string $list, ?ResourceListElement $after, ResourceListElement $element, bool $checkExistance ): void {
        if ( $checkExistance && $this->_resourceAlreadyExists( $list, $element ) )
            return;
        $element = new ResourceListElement( $element->getResource() );
        if ( $list == "import-list" ) {
            if ( $after == null ) {
                $element->setNext($this->importListFirst);
                if ( $this->importListFirst === null ) {
                    $this->importListFirst = $this->importListLast = $element;
                } else {
                    $this->importListFirst = $element;
                }
            } else {
                $element->setNext( $after->getNext() );
                $after->setNext( $element );
                if ( $after === $this->importListLast )
                    $this->importListLast = $element;
            }
        } else if ( $list == "temporary-list" ) {
            if ( $after == null ) {
                $element->setNext($this->temporaryList);
                $this->temporaryList = $element;
            } else {
                $element->setNext( $after->getNext() );
                $after->setNext( $element );
            }
        }
    }

    private function _removeResourceInTheList( string $list, ?ResourceListElement $previous, ResourceListElement $current ): void {
        if ( $list == "import-list" ) {
            if ( $previous != null ) {
                $previous->setNext($current->getNext());
                if ( $current === $this->importListLast )
                    $this->importListLast = $previous;
            } else {
                $this->importListFirst = $this->importListFirst->getNext();
                if ( $this->importListFirst == null ) {
                    $this->importListLast = null;
                };
            }
        } else if ( $list == "temporary-list" ) {
            if ( $previous != null ) {
                $previous->setNext($current->getNext());
            } else {
                //echo "DELETE FRIST (" . $this->temporaryList->getResource()->getId() . ")<br>";
                $this->temporaryList = $this->temporaryList->getNext();
            };
        }
    }

    /**
     * @param ResourceListElement $element
     * @param array $arr
     */
    private function _updateDependenciesInTemporaryList( ResourceListElement $element ): void {
        $previous = null;
        $current = $this->temporaryList;
        $elementId = $element->getResource()->getId();
        while( $current != null ) {
            //echo $current->getResource()->getId() . " " . $elementId . "<br>";
            if ( $current->getResource()->hasDependency( $elementId ) ) {
                $current->getResource()->removeDependency( $elementId );
                if ( $current->getResource()->getDependencyCount() == 0 ) {
                    //echo  $element->getResource() . "<br>";
                    $this->_removeResourceInTheList("temporary-list", $previous, $current);
                    $this->_addResourceInTheList("import-list", $this->importListLast, $current, false);
                    $this->_updateDependenciesInTemporaryList( $current );
                    $current = $current->getNext();
                } else {
                    $previous = $current;
                    $current = $current->getNext();
                }
            } else {
                $previous = $current;
                $current = $current->getNext();
            };
        };
    }

    private function _dump_list( string $list ): string {
        if ( $list == "import-list" ) {
            $current = $this->importListFirst;
        } else if ( $list == "temporary-list" ) {
            $current = $this->temporaryList;
        };
        $str = '***[';
        while( $current != null ) {
            $str .= '"' . $current->getResource() . '"';
            $current = $current->getNext();
            if ( $current != null )
                $str .= ', ';
        }
        return $str .= ']***';
    }

    private function _resourceAlreadyExists(string $list, ResourceListElement $element): bool {
        if ( $list == "import-list" ) {
            $current = $this->importListFirst;
        } else if ( $list == "temporary-list" ) {
            $current = $this->temporaryList;
        } else
            return false;
        while( $current != null ) {
            if ( strcmp( $current->getResource()->getId(), $element->getResource()->getId() ) == 0 )
                return true;
            $current = $current->getNext();
        }
        return false;
    }
}