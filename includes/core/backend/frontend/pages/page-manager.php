<?php

namespace Core;

if ( !defined('APP_REQ') )
    exit();

class PageManager {

    public const SECTION_ACCOUNTANT = 1;
    public const SECTION_RETAIL = 2;

    private static int $section = -1;
    private static ?Page $page = null;
    private static bool $loaded = false;

    public static function assignCurrentPage( Page $page ): void {
        if ( self::$page == null )
            self::$page = $page;
        else
            die('Multiple times assigning page');
    }

    public static function setSection( int $section ): void {
        self::$section = $section;
    }

    public static function getPage(): Page {
        return self::$page;
    }

    public static function pageExists(): bool {
        return self::$page != null;
    }

    public static function isDedicatedPage(): bool {
        return self::$section != -1;
    }

    public static function loadPageFromURL(): bool {

        if ( self::$loaded )
            return false;

        if ( !self::checkIfSectionExists() )
            return false;

        if ( IS_AJAX ) {
            try {
                $page = preg_split("/\//", AJAX_REQUEST_TARGET)[1];
                $page .=  "/";
            } catch( \Exception $e ) {
                return false;
            }
        } else {
            $page = isset($_GET['page']) ? $_GET['page'] : null;
            if (!$page)
                return false;
        }

        //$arr = preg_split("/", $page );
        $path = self::getSectionDirectoryURL() . "/" . $page . "includes.php";
        $b = file_exists( $path );

        if ( !$b )
            return false;

        require $path;
        self::$loaded = true;

        return true;
    }

    private static function checkIfSectionExists(): bool {
        if ( self::$section != self::SECTION_ACCOUNTANT
            && self::$section != self::SECTION_RETAIL
        )
            return false;
        return true;
    }

    private static function getSectionDirectoryURL(): ?string {
        switch (self::$section) {
            case self::SECTION_ACCOUNTANT:
                return S_ACCOUNTING_URL;
            case self::SECTION_RETAIL:
                return S_RETAIL_URL;
        }
        return null;
    }

    private static function getSectionDirectiryURI(): ?string {
        switch (self::$section) {
            case self::SECTION_ACCOUNTANT:
                return S_ACCOUNTING_URI;
            case self::SECTION_RETAIL:
                return S_RETAIL_URI;
        }
        return null;
    }

    /**
     * Write all data necessary for program execution.
     */
    public static function writeSystemVariable(): void {
        $obj = array();
        $obj["page"] = array();
        $obj = apply_filter( 'system_variable', $obj );
        echo json_encode( (object)$obj );
    }
}

