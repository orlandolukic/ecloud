<?php

namespace Core;

use JetBrains\PhpStorm\Pure;

if ( !defined('APP_REQ') )
    exit();

abstract class AuthorizedPage extends \Core\Page {

    #[Pure] public function canActivate(): bool
    {
        return is_user_logged_in();
    }

}
