<?php

namespace Core;

if ( !defined('APP_REQ') )
    exit();

/**
 * Class Page
 * @package Core
 */
abstract class Page {

    private string $name;
    private array $scriptResources;
    private array $styleResources;

    public function __construct( string $slug ) {

        $this->name = $slug;
        $this->scriptResources = array();
        $this->styleResources = array();

        add_action( 'init', 10, array( $this, 'init' ) );
        add_action( 'activation', 10, array( $this, 'canActivate' ) );
        add_action( 'page_content_html', 10, array( $this, 'printContent' ) );

        add_filter( 'ajax_get_page', array( $this, 'ajaxData' ));
        add_filter('system_variable', array( $this, 'updateSystemVariable' ) );
    }

    protected function addScriptResource( string $id, string $path, ?array $deps = null, ?string $version = null ): void {
        $res = new ScriptResource("page-" . $this->name . "-" . $id, S_ACCOUNTING_URI . "/resource/$this->name$path", $deps, $version);
        array_push( $this->scriptResources, $res );
    }

    protected function addStyleResource( string $id, string $path, ?array $deps = null, ?string $version = null ): void {
        $res = new StyleResource("page-" . $this->name . "-" . $id, S_ACCOUNTING_URI . "/resource/$this->name$path", $deps, $version);
        array_push( $this->styleResources, $res );
    }

    /**
     * Gets HTML template for this page.
     * @return string
     */
    public abstract function getTemplateUrl(): string;

    /**
     * @return bool Indicator which tells if user can access this page.
     */
    public abstract function canActivate(): bool;

    /**
     * On page init.
     */
    public abstract function init(): void;

    public abstract function getTitleTranslationKey(): string;

    /**
     * Returns JavaScript controller class for handling page requests
     *
     * @return string|null JavaScript class controller
     */
    public function getControllerName(): ?string {
        return null;
    }

    public final function ajaxData( array $arr ): array {
        ob_start();
        $this->printContent();
        $arr["content"] = ob_get_clean();
        $arr["page_title"] = __( $this->getTitleTranslationKey() );
        $arr["controller"] = $this->getControllerName();

        $this->packScripts($arr);
        return $arr;
    }

    private function packScripts( array &$arr ): void {
        $arr["scripts"] = array();
        $arr["styles"] = array();

        $scripts = &$arr["scripts"];
        $styles = &$arr["styles"];
        for ($i = 0; $i < count( $this->scriptResources ); $i++) {
            array_push( $scripts, $this->scriptResources[$i]->html() );
        }
        for ($i = 0; $i < count( $this->styleResources ); $i++) {
            array_push( $styles, $this->styleResources[$i]->html() );
        }
    }

    /**
     * @param \stdClass $variable
     * @return array changed system variable array.
     */
    public function updateSystemVariable( array $variable ): object {
        return (object)$variable;
    }

    /**
     * Prints content to the screen.
     */
    public function printContent(): void {
        if ( $this->canActivate() ) {
            include $this->getTemplateUrl();
        } else {
            echo "<h3>NOT ALLOWED</h3>";
        }
    }
}