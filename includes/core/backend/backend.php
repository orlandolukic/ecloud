<?php

namespace Core;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

// Connect main database, for connection purposes.
add_database("localhost", "ecloud", "ecloud", "IsKLaT3xx8w6HjGq");

function define_core_constants(): void {
    define("TEST_DATABASE_COLUMN", true);
}
add_action('define_constants', 10,'\Core\define_core_constants');