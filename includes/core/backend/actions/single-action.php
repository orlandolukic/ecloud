<?php
namespace Core;

use PDO;

if ( !defined("APP_REQ") )
    exit();

class SingleActionElement {
    private int $priority;
    private array|string $function;
    private self|null $next;

    public function __construct( int $priority, array|string $function )
    {
        $this->priority = $priority;
        $this->function = $function;
        $this->next = null;
    }

    public function getPriority(): int {
        return $this->priority;
    }

    public function getFunction(): array|string {
        return $this->function;
    }

    public function getNext(): self|null {
        return $this->next;
    }

    public function setNext( $next ): void {
        $this->next = $next;
    }
}

class SingleAction {
    private string $name;
    private SingleActionElement|null $firstAction;
    private int $callableNumber;

    public function __construct( string $name )
    {
        $this->firstAction = null;
        $this->name = $name;
        $this->callableNumber = 0;
    }

    public function executeAction( array $parameters ): void {
        $current = $this->firstAction;
        while( $current != null ) {
            if ( count($parameters) > 0 ) {
                call_user_func_array($current->getFunction(), $parameters);
            } else
                call_user_func(  $current->getFunction() );
            $current = $current->getNext();
        }
    }

    public function registerCallback( int $priority, array|string $function ): void {
        $elem = new SingleActionElement($priority, $function);
        if ( !$this->firstAction ) {
            $this->firstAction = $elem;
        } else {
            $previous = null;
            $current = $this->firstAction;
            while( $current != null && $current->getPriority() <= $priority ) {
                $previous = $current;
                $current = $current->getNext();
            }
            if ( $current == null ) {
                $previous->setNext( $elem );
            } else {
                $elem->setNext( $current );
                if ( $previous == null ) {
                    $elem->setNext( $this->firstAction );
                    $this->firstAction = $elem;
                } else
                    $previous->setNext( $elem );
            }
        };
        $this->callableNumber++;
    }

    public function deregisterCallaback( array|string $function ): void {
        $previous = null;
        $current = $this->firstAction;
        $deleted = false;
        while( $current != null && !$deleted ) {
            if ( $current->getFunction() == $function ) {
                if ( $previous == null ) {
                    $this->firstAction = $this->firstAction->getNext();
                } else
                    $previous->setNext( $current->getNext() );
                $deleted = true;
                $this->callableNumber--;
            };
            $previous = $current;
            $current = $current->getNext();
        }
    }

    public function getCallableNumber(): int {
        return $this->callableNumber;
    }

    public function getActionName(): string {
        return $this->name;
    }
}