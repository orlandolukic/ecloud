<?php

if ( !defined("APP_REQ") )
    exit();

require 'single-action.php';
require 'action-center.php';

function add_action(string $name, int $priority, array|string $function ): void {
    \Core\ActionCentar::getInstance()->registerCallable($name, $priority, $function);
}

function remove_action( string $name, array|string $function ): void {
    \Core\ActionCentar::getInstance()->deregisterCallable($name, $function);
}

function do_action( string $name, mixed ...$params ) {
    $args = func_get_args();
    $array = [];
    for ($i = 1; $i < count( $args ); $i++ ) {
        $array[$i - 1] = $args[$i];
    }
    \Core\ActionCentar::getInstance()->executeAction( $name, $array );
}

