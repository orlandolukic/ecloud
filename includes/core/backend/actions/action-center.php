<?php

namespace Core;

use JetBrains\PhpStorm\Pure;

if ( !defined("APP_REQ") )
    exit();

class ActionCeneterElement {
    private SingleAction $action;
    private ?self $next;

    public function __construct( SingleAction $action )
    {
        $this->action = $action;
        $this->next = null;
    }

    public function getAction(): SingleAction
    {
        return $this->action;
    }

    public function getNext(): ?ActionCeneterElement
    {
        return $this->next;
    }

    public function setNext(?ActionCeneterElement $next): void
    {
        $this->next = $next;
    }
}

class ActionCentar {

    private static ?self $instance = null;
    public static function getInstance(): self {
        if ( self::$instance == null )
            self::$instance = new self();
        return self::$instance;
    }

    private ?ActionCeneterElement $actionsFirst;
    private ?ActionCeneterElement $actionsLast;
    private int $actionsCount;

    private function __construct() {
        $this->actionsFirst = null;
        $this->actionsLast = null;
        $this->actionsCount = 0;
    }

    public function registerCallable( string $actionName, int $priority, array|string $function ): void {
        $action = $this->_hasActionInTheCenter($actionName);
        if ( $action != null ) {
            $action->registerCallback($priority, $function);
        } else {
            $newAction = new SingleAction($actionName);
            $newActionCenterElement = new ActionCeneterElement( $newAction );
            $newAction->registerCallback($priority, $function);
            if ( !$this->actionsFirst ) {
                $this->actionsFirst = $this->actionsLast = $newActionCenterElement;
            } else {
                $this->actionsLast->setNext($newActionCenterElement);
                $this->actionsLast = $newActionCenterElement;
            }
            $this->actionsCount++;
        }
    }

    public function deregisterCallable( string $actionName, array|string $function ): void {
        $action = $this->_hasActionInTheCenter($actionName);
        if ( $action != null ) {
            $action->deregisterCallaback( $function);
        };
    }

    public function executeAction( string $name, array $params ): void {
        $action = $this->_hasActionInTheCenter($name);
        if ( $action != null ) {
            $action->executeAction( $params );
        };
    }

    #[Pure] private function _hasActionInTheCenter(string $actionName ): ?SingleAction  {
        $action = null;
        $current = $this->actionsFirst;
        while( $current != null && $action == null ) {
            if ( strcmp( $current->getAction()->getActionName(), $actionName ) === 0 )
                $action = $current->getAction();
            $current = $current->getNext();
        }
        return $action;
    }

}