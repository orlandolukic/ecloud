<?php

namespace Core\DataStructures;

use Closure;
use JetBrains\PhpStorm\Pure;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class Stack {

    private ?ListElement $sp;
    private ?ListElement $iterator;
    private int $elements;

    public function __construct()
    {
        $this->sp = null;
        $this->elements = 0;
    }

    public function push( mixed $element ): void {
        $le = new ListElement($element);
        if ( !$this->sp ) {
            $this->sp = $le;
        } else {
            $le->setNext($this->sp);
            $this->sp = $le;
        }
        $this->elements++;
    }

    #[Pure] public function top(): mixed {
        return $this->sp ? $this->sp->getContent() : null;
    }

    public function pop(): mixed {
        if ( $this->sp ) {
            $el = $this->sp;
            $this->sp = $this->sp->getNext();
            return $el->getContent();
        } else
            return null;
    }

    public function forEach( array|string|closure $func ): void {
        $current = $this->sp;
        while( $current !== null ) {
            if ( $func instanceof closure ) {
                $func->call( $current->getContent() );
            } else {
                call_user_func( $func, $current->getContent() );
            };
            $current = $current->getNext();
        }
    }

    public function iterator_init(): void {
        $this->iterator = $this->sp;
    }

    public function iterator_has_current(): bool {
        return $this->iterator !== null;
    }

    public function iterator_get_current(): ?ListElement {
        return $this->iterator;
    }

    public function iterator_move_current(): void {
        if ( $this->iterator !== null )
            $this->iterator = $this->iterator->getNext();
    }
}