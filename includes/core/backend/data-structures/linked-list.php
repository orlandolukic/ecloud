<?php

namespace Core\DataStructures;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class LinkedList {
    private ?ListElement $first, $last, $prevLast;
    private int $elements;

    public function __construct()
    {
        $this->first = null;
        $this->last = null;
        $this->prevLast = null;
        $this->elements = 0;
    }

    public function addLast( mixed $content ): void {
        $newElement = new ListElement($content);
        if ( !$this->first ) {
            $this->first = $this->last = $newElement;
        } else {
            $this->last->setNext($newElement);
            $this->last = $newElement;
        }
        $this->elements++;
    }

    public function addFirst(  mixed $content ): void {
        $newElement = new ListElement($content);
        if ( !$this->first ) {
            $this->first = $this->last = $newElement;
        } else {
            $newElement->setNext($this->first);
            $this->first = $newElement;
        };
        $this->elements++;
    }

    public function removeFirst(): mixed {
        if ( $this->first ) {
            $el = $this->first->getContent();
            $this->first = $this->first->getNext();
            if ( !$this->first )
                $this->last = null;
            $this->elements--;
            return $el;
        } else
            return null;
    }

    public function removeLast(): mixed {
        $current = $this->first;
        $previous = null;
        while( $current !== $this->last ) {
            $previous = $current;
            $current = $current->getNext();
        }
        if ( $previous === null ) {
            $el = $this->first;
            $this->first = null;
            $this->last = null;
        } else {
            $el = $this->last;
            $this->last = $previous;
        }
        $this->elements--;
        return $el;
    }

    public function get( int $pos ): mixed {
        $current = $this->first;
        $i = 0;
        $elem = null;
        while( $current !== null && $i < $pos ) {
            $i++;
            $current = $current->getNext();
        }
        if ( $i == $pos ) {
            $elem = $current;
        };
        return $elem ? $elem->getContent() : null;
    }

    #[Pure] private function _hasActionInTheCenter(string $actionName ): mixed  {
        $current = $this->first;
        while( $current != null ) {
            /*
            if ( strcmp( $current->getAction()->getActionName(), $actionName ) === 0 )
                $action = $current->getAction();
            */
            $current = $current->getNext();
        }
        return null;
    }

    public function size(): int {
        return $this->elements;
    }
}