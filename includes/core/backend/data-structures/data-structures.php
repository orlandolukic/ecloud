<?php

namespace Core\DataStructures;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

require 'stack.php';
require 'list-element.php';
require 'linked-list.php';