<?php

namespace Core\DataStructures;

// Silence is golden!
if ( !defined("APP_REQ") )
    exit();

class ListElement {
    private mixed $content;
    private ?ListElement $next;

    public function __construct( mixed $content )
    {
        $this->content = $content;
        $this->next = null;
    }

    /**
     * @return mixed
     */
    public function getContent(): mixed
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return ListElement
     */
    public function setContent(mixed $content): ListElement
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return ListElement|null
     */
    public function getNext(): ?ListElement
    {
        return $this->next;
    }

    /**
     * @param ListElement|null $next
     * @return ListElement
     */
    public function setNext(?ListElement $next): ListElement
    {
        $this->next = $next;
        return $this;
    }


}