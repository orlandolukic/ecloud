<?php

if ( !defined('APP_REQ') )
    exit();

class AccountantPages {

    public const CAT_REGISTRY = "registry";
    public const CAT_FINANCE = "finance";
    public const CAT_EXPORT_IMPORT = "export-import";
    public const CAT_WHOLESALE = "wholesale";
    public const CAT_RETAIL = "retail";

    public const LOGIN = "login";

    public const REGISTRY_OFFICIALS = self::CAT_REGISTRY . "/officials";
    public const REGISTRY_STORAGE_UNITS = self::CAT_REGISTRY . "/storage-units";
    public const REGISTRY_CLIENTS = self::CAT_REGISTRY . "/clients";
    public const REGISTRY_CONTACTS = self::CAT_REGISTRY . "/clients";
    public const REGISTRY_GOODS = self::CAT_REGISTRY . "/goods-and-services";
    public const REGISTRY_QR = self::CAT_REGISTRY . "/qr";
    public const REGISTRY_GOODS_GROUPS = self::CAT_REGISTRY . "/goods-group";
    public const REGISTRY_CLIENTS_GROUP = self::CAT_REGISTRY . "/clients-group";
    public const REGISTRY_MANUFACTURERS = self::CAT_REGISTRY . "/manufacturers";
    public const REGISTRY_ORGANIZATION_UNITS = self::CAT_REGISTRY . "/organization-units";
    public const REGISTRY_REVENUE_EXPENSE_PLACES = self::CAT_REGISTRY . "/revenue-and-expense-places";
    public const REGISTRY_EXCHANGE_RATES = self::CAT_REGISTRY . "/exchange-rates";
    public const REGISTRY_TEXTS = self::CAT_REGISTRY . "/texts";
    public const REGISTRY_EMPLOYEES = self::CAT_REGISTRY . "/employees";
    public const REGISTRY_FIXED_ASSETS = self::CAT_REGISTRY . "/fixed-assets";
    public const REGISTRY_FIXED_ASSETS_GROUPS = self::CAT_REGISTRY . "/fixed-assets-groups";
    public const REGISTRY_GOODS_LIST = self::CAT_REGISTRY . "/goods-list";

}