<?php

if ( !defined( 'APP_REQ' ) )
    exit();

function page_redirect( string $url ): void {
    if ( IS_AJAX ) {
        function _redirectAjax($arr) {
            $arr["redirect"] = '/not-found';
            return $arr;
        }
        add_filter('ajax_' . AJAX_REQUEST, '_redirectAjax', 20);
    } else {
        header('Location: ' . ROOT_URI . $url);
        exit();
    }
}

function check_page_existance() {
    \Core\PageManager::loadPageFromURL();
    if ( \Core\PageManager::isDedicatedPage() && !\Core\PageManager::pageExists() )
        page_redirect('/not-found');
}
add_action( 'redirection', 0, 'check_page_existance' );

if ( IS_AJAX ) {
    function _dontRedirectAjax($arr) {
        $arr["redirect"] = false;
        return $arr;
    }
    add_filter('ajax_' . AJAX_REQUEST, '_dontRedirectAjax');
}