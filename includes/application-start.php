<?php
/**
 * ===========================================================
 *      APPLICATION START (include all services)
 * ===========================================================
 */

/**
 * Load core
 */
require "constants.php";
require "core/backend/data-structures/data-structures.php";
require "core/backend/actions/action-manager.php";
require "core/backend/filters/filter-manager.php";
require "core/backend/locales/locales.php";
require "core/backend/frontend/frontend.php";
require "core/backend/database/database.php";
require "core/backend/tests/includes.php";
require "core/utils/redirection.php";
require "core/users/includes.php";

// Require all code written for core.
require "core/backend/backend.php";

// Define all constants important for the system.
do_action( 'define_constants' );

// Include all user written code.
require S_ACCOUNTING_URL . '/includes.php';

// Handle redirection requests.
do_action('redirection' );

// Initialize system.
do_action('init');

// Connects all databases to the system.
do_action('core_connect_databases');

// Disconnect all databases
do_action('core_disconnect_databases');