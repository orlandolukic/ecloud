<?php

/**
 * Define all constants for application
 */

define( "ROOT_URI", "https://ecloud.rs" );
define( "ROOT_URL", $_SERVER['DOCUMENT_ROOT'] );

define( "INCLUDES_URI", "https://ecloud.rs/includes" );
define( "INCLUDES_URL", $_SERVER['DOCUMENT_ROOT'] . "/includes" );

define( "LOCALES_URI", "https://ecloud.rs/locales" );
define( "LOCALES_URL", $_SERVER['DOCUMENT_ROOT'] . "/locales" );

define( "S_ACCOUNTING_URI", "https://ecloud.rs/accountant" );
define( "S_ACCOUNTING_URL", $_SERVER['DOCUMENT_ROOT'] . "/for/accountant" );

define( "S_RETAIL_URI", "https://ecloud.rs/retail" );
define( "S_RETAIL_URL", $_SERVER['DOCUMENT_ROOT'] . "/for/retail" );

define( "ASSETS_URI", "https://ecloud.rs/assets" );
define( "ASSETS_URL", $_SERVER['DOCUMENT_ROOT'] . "/assets" );

define( "APP_REQ", 1 );