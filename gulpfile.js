var gulp = require('gulp');
var sass = require('gulp-sass');
var replace = require('gulp-replace');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gulpDocumentation = require("gulp-documentation");

let forPath = "";
let forFileName = "";
let forPathDest = "";

gulp.task('sass', function() {
    //root scss file (import all your partials into here)
    return gulp.src('./assets/scss/business.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        // add vendor prefixes
        .pipe(autoprefixer())
        // change the file name to be styles.scss.liquid file
        .pipe(rename('business-min.css'))
        // save the file to the theme assets directory
        .pipe(gulp.dest('./assets/css/'));
});

gulp.task('core-each', function() {

    gulp.src('./includes/core/frontend/**/*.js')
        .pipe(gulpDocumentation('html', { polyglot: true }, {
            name: 'My Project',
            version: '1.0.0'
        }))
        .pipe(gulp.dest('html-documentation/'));

    return gulp.src('./includes/core/frontend/core.js')
        .pipe( gulp.src(['./includes/core/frontend/**/*.js', '!./includes/core/frontend/core.js']) )
        .pipe( gulp.src('./for/**/*.js') )
        .pipe(concat('core.js'))
        .pipe(uglify())
        .pipe(rename({suffix: '-min'}))
        .pipe(gulp.dest('./assets/js/'));
});

gulp.task('accountant-style-each', function() {
    let x = gulp.src(forPath + "/" + forFileName + ".scss");
    return x.pipe(sass({
        outputStyle: 'compressed',
        includePaths: ['./assets/scss']
    }).on('error', sass.logError))
        .pipe( autoprefixer() )
        .pipe( gulp.dest(forPathDest) );
});

gulp.task('default', function() {
    gulp.watch(['./assets/scss/*.scss'], gulp.series('sass'));
    let watcher = gulp.watch(['./for/**/*.scss'], gulp.series('accountant-style-each'));
    watcher.on('change', function(path, stats) {
        forPath = path;
        forPath = forPath.split("\\");
        forFileName = forPath[ forPath.length-1 ];
        forFileName = forFileName.replace( /\.scss/g, "" );
        let x = "./";
        let y = "./";
        let n = forPath.length-1;
        for (let i = 0; i<n; i++) {
            x += forPath[i];
            if ( forPath[i] !== "scss" ) {
                y += forPath[i];
                if ( i+1<n && forPath[i+1] !== "scss" )
                    y += "/";
            }
            if ( i + 1 < n )
                x += "/";
        }
        forPath = x;
        forPathDest = y;
    });
    gulp.watch(['./includes/core/frontend/**/*.js'], gulp.series('core-each'));
    gulp.watch(['./for/**/*.js'], gulp.series('core-each'));
});
