<?php

// Silence is golden!
if (
    !isset( $_GET ) || !isset( $_GET['request'] )
    || !isset( $_POST ) || !isset( $_POST['pathname'] )
) {
    exit();
};

/**
 * Start of the application
 */
// Define AJAX environment.
define( "IS_AJAX", true );

// Define current ajax request.
$t = $_GET['request'];
$t = str_replace("-", "_", $t );
define( "AJAX_REQUEST", $t );

// Define ajax's target.
define( "AJAX_REQUEST_TARGET", $_POST['pathname'] );

// Include all core.
include "../includes/application-start.php";

// Perform actions before handling response object.
do_action( "ajax_before", AJAX_REQUEST );

// Apply filter for this request.
$array = apply_filter( "ajax_general", array() );

// Apply filter to current ajax request.
$array["request"] = apply_filter( "ajax_" . AJAX_REQUEST, array() );

// Perform actions after handling response object.
do_action( "ajax_after", AJAX_REQUEST, $array );

echo json_encode((object) $array);

